package ifisol.fitxchange.Controller.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ifisol.fitxchange.View.Fragments.DemoFragment;
import ifisol.fitxchange.View.Fragments.DemoThreeFragment;
import ifisol.fitxchange.View.Fragments.DemoTwoFragment;

/**
 * Created by MEH on 6/20/2016.
 */
public class DemoAdapter extends FragmentPagerAdapter {
    public DemoAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new DemoFragment();
            case 1:
                return new DemoThreeFragment();
             default:
                 return new DemoTwoFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
