package ifisol.fitxchange.Controller.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ifisol.fitxchange.Controller.Async.GetTokenTask;
import ifisol.fitxchange.Controller.Async.SendNonceTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Subscription_Plans;
import ifisol.fitxchange.R;
import ifisol.fitxchange.View.Fragments.SettingsFragment;

/**
 * Created by MEH on 6/22/2016.
 */
public class PlansAdapter extends BaseAdapter {
    Context mContext;
    PlansAdapter plansAdapter;
    GlobalActivity mGlobal;
    SettingsFragment mFragment;
    public ArrayList<Subscription_Plans> mArray=new ArrayList<>();


    public PlansAdapter(Context context, ArrayList<Subscription_Plans> mArray, SettingsFragment fragment)
    {
        this.mContext=context;
        this.mArray=mArray;
        plansAdapter=this;
        mFragment = fragment;
        mGlobal = (GlobalActivity) mContext.getApplicationContext();

    }
    @Override
    public int getCount() {
        return mArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row=inflater.inflate(R.layout.plan_list_item,null);

        return initializeViews(mArray.get(position), row);
    }

    private View initializeViews(final Subscription_Plans obj,View view) {

        TextView planTitle      = (TextView) view.findViewById(R.id.plan_title);
        TextView tvPrice        = (TextView) view.findViewById(R.id.tv_price);
        TextView tvDuration     = (TextView) view.findViewById(R.id.tv_duration);
        TextView tvDescription  = (TextView) view.findViewById(R.id.tv_description);
        ImageView imgTick       = (ImageView) view.findViewById(R.id.img_tick);
        Button btnSubscribe     = (Button) view.findViewById(R.id.btn_subscribe);

        planTitle.setText(obj.getPlan_name());
        tvPrice.setText("$"+obj.getPlan_price()+" per month");
        tvDuration.setText(obj.getPlan_duration()+" Months");
        tvDescription.setText(obj.getPlan_description());

        if (obj.getIs_subscribed().equalsIgnoreCase("yes"))
        {
            imgTick.setVisibility(View.VISIBLE);
            if (obj.getCan_un_subscribe().equalsIgnoreCase("no"))
            {
                btnSubscribe.setVisibility(View.GONE);
            }
            else
            {
                btnSubscribe.setVisibility(View.VISIBLE);
                btnSubscribe.setText("UnSubscribe");
            }

        }
        else
        {
            imgTick.setVisibility(View.GONE);
            btnSubscribe.setText("Subscribe");
        }

        btnSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (obj.getIs_subscribed().equalsIgnoreCase("yes"))
                {
                    ConfirmationDiaolog(obj);
                }
                else
                {

                    String plainName="";
                    for (Subscription_Plans obj:mArray)
                    {
                        if (obj.getIs_subscribed().equalsIgnoreCase("yes"))
                        {
                            plainName = obj.getPlan_name();
                            break;
                        }
            }

                    if (plainName.equals(""))
                    {
                        new GetTokenTask(mFragment,obj).execute();
                    }
                    else
                        SHOWDialog(plainName);

                }

            }
        });


        return view;
    }

    private void ConfirmationDiaolog(final Subscription_Plans mobj) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_unregister_user);
        DisplayMetrics dm = new DisplayMetrics();
        mFragment.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        TextView tvMessage=(TextView)dialog.findViewById(R.id.tv_message);
        tvMessage.setText("Do u really want to unsubscribe");
        final EditText edt_mail=(EditText)dialog.findViewById(R.id.edt_eemail);
        Button mBttonok = (Button) dialog.findViewById(R.id.bt_ok);
        Button mBttonCancel = (Button) dialog.findViewById(R.id.bt_cancel);

        mBttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* HashMap<String, String> param = new HashMap<>();
                {
                    String user_id=mGlabal_obj.GetActiveUser().getUser_id();
                    param.put("user_id", user_id);
                    new UnRegisterUserTask(mActivity,param).execute();
                    dialog.dismiss();
                }*/
                mFragment.plansObj=mobj;
                new SendNonceTask(mFragment).execute("", mGlobal.GetActiveUser().getUser_id(), mobj.getPlan_id(),"In-Active");
                dialog.dismiss();
            }
        });
        mBttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void SHOWDialog(String plan) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_status_alert_login);
        DisplayMetrics dm = new DisplayMetrics();
        mFragment.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        TextView mTv = (TextView) dialog.findViewById(R.id.R2);
        mTv.setText("You have already subscribed for "+plan+" please try again when current plan expires or cancel it to subscribe for a new plan. Thanks");
        Button btnOK = (Button) dialog.findViewById(R.id.bt_ok);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        dialog.show();
    }

}
