package ifisol.fitxchange.Controller.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.R;

/**
 * Created by MEH on 6/22/2016.
 */
public class DiscoverAdopter extends BaseAdapter {
    Context mContext;
    public ArrayList<Gym>mArray=new ArrayList<Gym>();
    GlobalActivity mGlobal;

    ArrayList<ImageView> mIVlist;

    ImageView mImg1;
    ImageView mImg2;
    ImageView mImg3;
    ImageView mImg4;
    ImageView mImg5;
    ImageView mImg6;
    ImageView mImg7;
    ImageView mImg8;
    ImageView mImg9;
    ImageView mImg10;
    ImageView mImg11;


    public DiscoverAdopter(Context context,ArrayList<Gym>mArray)
    {
        this.mContext=context;
        this.mArray=mArray;
        mGlobal = (GlobalActivity) mContext.getApplicationContext();

    }
    @Override
    public int getCount() {
        return mArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row=inflater.inflate(R.layout.custom_discover_jym_item,null);
        final TextView textView=(TextView)row.findViewById(R.id.tv_jym_name);
        final ImageView imgBanner=(ImageView)row.findViewById(R.id.img_banner_home);
       // View closeView=(View)row.findViewById(R.id.view_list);
        final TextView tv_Gym_Location=(TextView)row.findViewById(R.id.tv_gym_loction);
        TextView tvCloseGym=(TextView)row.findViewById(R.id.tv_close_gym);
        ImageView ImgHome=(ImageView)row.findViewById(R.id.img_home);
        final ImageView gymProfile=(ImageView)row.findViewById(R.id.jymprofile_image);
        Gym gym=mArray.get(position);
        textView.setText(gym.getGym_name());
        String distance=gym.getDistance_in_km();
        String value =String.format("%.2f", Double.valueOf(distance));

        if (value==null||value.equals(""))
        {
            tv_Gym_Location.setText("Distance not available");
        }
        else
        {
            float dis = Float.valueOf(value);
            if (dis<1)
            {
                int dist = Math.round(dis * 1000);
                tv_Gym_Location.setText(String.valueOf(dist) + " m");
            }
            else
                tv_Gym_Location.setText(value + " km");

        }
        if (!gym.getGym_logo().equals("")&&gym.getGym_logo()!=null) {
            Picasso.with(mContext)
                    .load(Constants.Img_URL + gym.getGym_logo())
                    .error(R.drawable.ic_placholdr)
                    .into(gymProfile);
        }
        else
            gymProfile.setImageResource(R.drawable.ic_placholdr);

        if (mGlobal.GetActiveUser()!=null) {
            if (mGlobal.GetActiveUser().getGym_id().equals(gym.getGym_id())) {
                imgBanner.setVisibility(View.VISIBLE);
            } else imgBanner.setVisibility(View.GONE);
        }


         mIVlist = new ArrayList<>();

        mImg1 =  (ImageView)  row.findViewById(R.id.img_1);
        mImg2 =  (ImageView)  row.findViewById(R.id.img_2);
        mImg3 =  (ImageView)  row.findViewById(R.id.img_3);
        mImg4 =  (ImageView)  row.findViewById(R.id.img_4);
        mImg5 =  (ImageView)  row.findViewById(R.id.img_5);
        mImg6 =  (ImageView)  row.findViewById(R.id.img_6);
        mImg7 =  (ImageView)  row.findViewById(R.id.img_7);
        mImg8 =  (ImageView)  row.findViewById(R.id.img_8);
        mImg9 =  (ImageView)  row.findViewById(R.id.img_9);
        mImg10 = (ImageView) row.findViewById(R.id.img_10);
        mImg11 = (ImageView) row.findViewById(R.id.img_11);

        mImg1. setTag("boxing.png");
        mImg2. setTag("cardio.png");
        mImg3.setTag("free_weights.png");
        mImg4.setTag("machine_weights.png");
        mImg5. setTag("pool.png");
        mImg6. setTag("wom_only.png");
        mImg7.setTag("x_fit.png");
        mImg8.setTag("yoga.png");
        mImg9.setTag("other.png");
        mImg10.setTag("group_fitness.png");
        mImg11.setTag("functional_training.png");


        mImg1. setVisibility(View.GONE);
        mImg2. setVisibility(View.GONE);
        mImg3. setVisibility(View.GONE);
        mImg4. setVisibility(View.GONE);
        mImg5. setVisibility(View.GONE);
        mImg6. setVisibility(View.GONE);
        mImg7. setVisibility(View.GONE);
        mImg8. setVisibility(View.GONE);
        mImg9. setVisibility(View.GONE);
        mImg10.setVisibility(View.GONE);
        mImg11.setVisibility(View.GONE);


        mIVlist.add(mImg1);
        mIVlist.add(mImg2);
        mIVlist.add(mImg3);
        mIVlist.add(mImg4);
        mIVlist.add(mImg5);
        mIVlist.add(mImg6);
        mIVlist.add(mImg7);
        mIVlist.add(mImg8);
        mIVlist.add(mImg9);
        mIVlist.add(mImg10);
        mIVlist.add(mImg11);

       // View viewList=(View)row.findViewById(R.id.view_list);


                for(int i=0; i<mArray.get(position).getGym_features().size();i++)
                {
                    String img  = mArray.get(position).getGym_features().get(i).getFeature_img();
                    for (ImageView obj: mIVlist)
                    {
                        if (img.equalsIgnoreCase(obj.getTag().toString()))
                        {
                            obj.setVisibility(View.VISIBLE);
                        }
                    }
                }

      /*  String[] indexes = gym.getGym_feature().replaceAll("\\s","").split(",");
        for (int i=0 ; i<indexes.length; i ++)
        {
            if (Integer.parseInt(indexes[i])<9)
               mIVlist.get(Integer.parseInt(indexes[i])).setVisibility(View.VISIBLE);
        }*/



       /* if (obj.isChecked)
        {
            view.setVisibility(View.VISIBLE);
        }
        else
        {
            view.setVisibility(View.GONE);
        }*/
        textView.setText(gym.getGym_name());
        Calendar calendar = Calendar.getInstance();
        String dayName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
        for (int i = 0; i < gym.getGym_timings().size(); i++) {
            if (gym.getGym_timings().get(i).getDay().equalsIgnoreCase(dayName)) {
                String openingTime = gym.getGym_timings().get(i).getOpen_timing();
                String closingTime = gym.getGym_timings().get(i).getClose_timing();
                if (openingTime.equals("")||closingTime.equals(""))
                {
                    tvCloseGym.setText("Closed today");
                }
            }
        }
       // tv_Gym_Location.setText(gym.getGym_location());



        return row;
    }
}
