package ifisol.fitxchange.Controller.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.R;

/**
 * Created by MEH on 6mGymList/4/2016.
 */
public class GymListingAdapter extends BaseAdapter {

    Activity context;
    public ArrayList<Gym> mGymList ;


    public GymListingAdapter(Activity mContext, ArrayList<Gym> mList)
    {
        context=mContext;
        mGymList=mList;

    }
    @Override
    public int getCount() {
        return mGymList.size();
    }

    @Override
    public Object getItem(int position) {
        return mGymList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getId(int position) {
        return mGymList.get(position).getGym_id();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        if (convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=inflater.inflate(R.layout.spinner_row_gym_name,null);

        }
        else
           row=convertView;

        TextView mTvTitle = (TextView) row.findViewById(R.id.tvgym_name);

        mTvTitle.setText(mGymList.get(position).getGym_name());


        return row;
    }
}
