package ifisol.fitxchange.Controller.Adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import ifisol.fitxchange.View.Fragments.CodeCurrentFragment;
import ifisol.fitxchange.View.Fragments.CodePreviousFragment;

/**
 * Created by MEH on 6/24/2016.
 */
public class CodePagerAdopter extends FragmentPagerAdapter {

    ArrayList<Fragment> mFragList;
    public CodePagerAdopter(FragmentManager fragmentManager){
        super(fragmentManager);
        mFragList=new ArrayList<>();
        mFragList.add(new CodeCurrentFragment());
        mFragList.add(new CodePreviousFragment());
    }


    @Override
    public Fragment getItem(int position) {
        return mFragList.get(position);
    }


    @Override
    public int getCount() {
        return mFragList.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        String title=" ";
        switch (position){
            case 0:
                title="CURRENT";
                break;
            case 1:
                title="PREVIOUS";
                break;
        }
        return title;
    }
}

