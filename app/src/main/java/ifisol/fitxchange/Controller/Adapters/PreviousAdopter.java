package ifisol.fitxchange.Controller.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.R;

/**
 * Created by MEH on 8/22/2016.
 */
public class PreviousAdopter extends BaseAdapter {
    ArrayList<Gym> array=new ArrayList<Gym>();
    Context mContext;
    public PreviousAdopter(ArrayList<Gym> arrray,Context context)
    {
        array=arrray;
        mContext=context;
    }
    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public Object getItem(int position) {
        return array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v= null;

        if (convertView==null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.custom_code_previous_item, null);
        }
        else
          v = convertView;


        TextView tvJymName          = (TextView)v.findViewById(R.id.tv_jym_name);
        TextView tv_address         = (TextView)v.findViewById(R.id.tv_adress);
        TextView tv_date            = (TextView)v.findViewById(R.id.tv_date);
        final ImageView profile     =(ImageView)v.findViewById(R.id.jym_profile_image);

        Gym gym = new Gym();
        gym = array.get(position);
        String[] date=gym.getCode_Date_Time_Creation().split("\\s+");
        tvJymName.setText(gym.getGym_name());
        tv_address.setText(gym.getGym_address());
        tv_date.setText(date[0]);
        if (!profile.equals("")&&profile!=null) {
            Picasso.with(mContext)
                    .load(Constants.Img_URL+gym.getGym_logo())
                    .into(profile);
        }

        return v;
    }
}
