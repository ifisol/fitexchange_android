package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.View.Activities.VerificationCodeActivity;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class UnRegisterUserTask extends AsyncTask<String, Void, String> {
    VerificationCodeActivity mActivity;
    Context mContext;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameter;
    public UnRegisterUserTask(VerificationCodeActivity activity, HashMap<String, String> params) {
        mActivity = activity;
        mContext = activity;
        mGlobal = (GlobalActivity) activity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
         mActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        parameter=params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameter, Constants.DELETE_USER);
        Log.d("Login Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            if (jsonObject.getString("status").equals("true"))
            {
               // Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                mActivity.API_RESULT_DELETE_USER();

            }
            else {
                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
