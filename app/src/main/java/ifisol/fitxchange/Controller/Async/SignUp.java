package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Users;
import ifisol.fitxchange.View.Activities.RegisterActivity;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class SignUp extends AsyncTask<String, Void, String> {
    RegisterActivity mActivity;
    Context mContext;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;

    public SignUp(RegisterActivity activity, HashMap<String, String> params) {
        mActivity = activity;
        mContext = activity;
        mGlobal = (GlobalActivity) activity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        mActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        parameters = params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        parameters.put("deviceToken",GetGcmId());
        Log.e("Signup Params>>>>>", parameters.toString());
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.SIGNUP);

        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {
                    Gson gson = new Gson();
                    Users mUser = gson.fromJson(jsonObject.getString("response"), Users.class);
                    if (mUser.getVerification_status().equalsIgnoreCase("Pending")) {
                        mActivity.Reigister_API_Status();
                    }
                    if (mUser.getVerification_status().equalsIgnoreCase("Gym_Approved")) {
                        mActivity.Reigister_API(mUser);
                    }

                } else
                {
                    Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }
}

    private String GetGcmId() {

        try {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mActivity);
            return gcm.register("378874944053");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }
}
