package ifisol.fitxchange.Controller.Async;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Fragments.SettingsFragment;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class SendNonceTask extends AsyncTask<String, Void, String> {
    Activity mActivity;
    Context mContext;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    SettingsFragment mFragment;
    String status;


    public SendNonceTask(SettingsFragment fragment) {
        mFragment = fragment;
        mActivity = mFragment.getActivity();
        mGlobal = (GlobalActivity)mActivity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        ((MenuActivity)mActivity).mViewProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        HashMap<String, String> prms = new HashMap();
        prms.put("nounce_id", params[0]);
        prms.put("user_id", params[1]);
        prms.put("plan_id", params[2]);
        prms.put("status", params[3]);
        status = params[3];
        resp = this.jsonArrayParser.getJSONFromUrl_POST(prms, Constants.CreateSubscription);

        Log.d("Send Token Params", params.toString());
        Log.d("Send Token Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        ((MenuActivity)mActivity).mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
        if (!result.isEmpty()||result!=null) {

                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {

                    mFragment.PaymentAPI(status);
                }
            else
                    Toast.makeText(mActivity,jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            }


            } catch (Exception e) {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();

        }

    }
}
