package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Fragments.DiscoverFragment;
import ifisol.fitxchange.View.Fragments.OwnGymDetailFragment;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class Get_Gym_info_Task extends AsyncTask<String, Void, String> {
    MenuActivity mActivity;
    Context mContext;
    private String resp;
    JSONArrayParser jsonArrayParser;
    OwnGymDetailFragment ownGymDetailFragment;


    public Get_Gym_info_Task(MenuActivity activity, OwnGymDetailFragment fragment) {

        mActivity = activity;
        jsonArrayParser = new JSONArrayParser();
        MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        ownGymDetailFragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        HashMap<String,String> parameters=new HashMap<>();

        parameters.put("user_id",params[0]);
        parameters.put("gym_id",params[1]);
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters,Constants.Gym_Info);
        Log.d("gym params > > > > >", parameters.toString());
        Log.d("gym Response > > > > > ", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        JSONObject jsonObject=null;
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        try {
            if (!result.isEmpty()||result!=null)
            {
                jsonObject = new JSONObject(result);
                ArrayList<Gym> gymList;
                if (jsonObject.getString("status").equals("true")) {
                    Gson gson = new Gson();
                    Gym gym = gson.fromJson(jsonObject.getString("response"), Gym.class);
                    ownGymDetailFragment.InitData(gym);

                }


            }
        }
        catch (Exception e)
        {
           e.printStackTrace();
        }

    }
}
