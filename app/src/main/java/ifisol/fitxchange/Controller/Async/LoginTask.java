package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Users;
import ifisol.fitxchange.View.Activities.LoginActivity;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class LoginTask extends AsyncTask<String, Void, String> {
    LoginActivity mActivity;
    Context mContext;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;

    public LoginTask(HashMap<String, String> params, LoginActivity activity,Context mCont) {
        mActivity = activity;
        mContext = mCont;
        mGlobal = (GlobalActivity) activity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        mActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        parameters = params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        parameters.put("deviceToken",GetGcmId());
        Log.e("Login Params>>>>>", parameters.toString());
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.LOGIN);

        Log.d("Login Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
        if (!result.isEmpty()) {

                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {
                    Gson gson = new Gson();
                    Users mUser = gson.fromJson(jsonObject.getString("response"),Users.class);
                    if (mUser.getVerification_status().equalsIgnoreCase("Pending"))
                    {
                        mActivity.Api_Result_Status();
                    }
                    if (mUser.getVerification_status().equalsIgnoreCase("Gym_Approved"))
                    {
                        mActivity.Api_Result_toRegisterActivity(mUser);
                    }

                    else if (mUser.getVerification_status().equalsIgnoreCase("Verified"))
                    {
                        mActivity.Api_Result(mUser);


                    }

                } else
                {
                    Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (Exception e)
         {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }

    }

    private String GetGcmId() {

        try {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mActivity);
            return gcm.register("378874944053");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }
}
