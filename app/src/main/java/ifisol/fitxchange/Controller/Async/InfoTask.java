package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Fragments.ProfileFragment;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class InfoTask extends AsyncTask<String, Void, String> {
    MenuActivity mActivity;
    Context mContext;
    private String resp;
    ProfileFragment profileFragment;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;

    public InfoTask(MenuActivity activity,ProfileFragment fra) {
        mActivity = activity;
        mContext = activity;
        profileFragment=fra;
        mGlobal = (GlobalActivity) activity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(new HashMap<String, String>(), Constants.INFO);

        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        JSONObject jsonObject = null;
        try {
                if (!result.isEmpty()||result!=null) {
                    jsonObject = new JSONObject(result);
                    if (jsonObject.getString("status").equals("true")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                        String url = jsonObject1.getString("video_url");
                            profileFragment.API_RESULT_INFO(url);
                    } else {
                        Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JSONException e)
            {
                mActivity.mViewProgressBar.setVisibility(View.GONE);
                Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
            }
    }
}
