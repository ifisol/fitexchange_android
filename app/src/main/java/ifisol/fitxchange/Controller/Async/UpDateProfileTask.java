package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Users;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Fragments.ProfileFragment;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 8/2/2016.
 */
public class UpDateProfileTask extends AsyncTask<String, Void, String> {
    MenuActivity mActivity;
    Context mContext;
    ProfileFragment mProfil;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;

    public UpDateProfileTask(HashMap<String, String> params, MenuActivity activity,ProfileFragment profilFragment) {
        mActivity = activity;
        mContext = activity;
        mProfil=profilFragment;
        mGlobal = (GlobalActivity) activity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        parameters = params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.UPDATE_Profile);

        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty() || result != null) {
                result = result.replaceAll("\\[", "").replaceAll("\\]", "");
                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {
                    Gson gson = new Gson();
                    Users mUser = gson.fromJson(jsonObject.getString("response"), Users.class);
                    mGlobal.SaveActiveUser(null);
                    mGlobal.SaveActiveUser(mUser);
                    mProfil.UPDATE_API_RESULT();
                } else {
                    Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }

    } catch (JSONException e) {
                MenuActivity.mViewProgressBar.setVisibility(View.GONE);
                Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();

        }
        }
}