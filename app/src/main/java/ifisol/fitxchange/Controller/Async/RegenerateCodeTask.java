package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Fragments.CodeCurrentFragment;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 9/22/2016.
 */
public class RegenerateCodeTask extends AsyncTask<String, Void, String> {
    MenuActivity mActivity;
    Context mContext;
    CodeCurrentFragment currentFragment;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameter;
    public RegenerateCodeTask(MenuActivity activity,CodeCurrentFragment fragment, HashMap<String, String> params) {
        mActivity = activity;
        currentFragment=fragment;
        mContext = activity;
        mGlobal = (GlobalActivity) activity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        parameter=params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameter, Constants.TRAIN_NOW);
        Log.d("Login Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            if (jsonObject.getString("status").equals("true"))
            {
                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                String gymCode=jsonObject.getString("gym_code");
                currentFragment.TRAIN_NOW_API_RESULT(gymCode);
            }
            else {
                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }
    }
}
