package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Fragments.DiscoverFragment;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class SearchGymTask extends AsyncTask<String, Void, String> {
    MenuActivity mActivity;
    Context mContext;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    DiscoverFragment mDiscoverFragment;

    public SearchGymTask(MenuActivity activity, DiscoverFragment discoverFragment, String progressbar,Context context) {
        mActivity = activity;
        mContext=context;
        mDiscoverFragment=discoverFragment;
        mGlobal = (GlobalActivity) mActivity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        if (progressbar.equalsIgnoreCase("progressbar_no"))
        {
            MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        }
        else  if (progressbar.equalsIgnoreCase("progressbar_yes"))
        {
            MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        HashMap<String,String> parameters=new HashMap<>();

        if (mGlobal.getLocation()!=null && mGlobal.getLocation().length()>0)
        {
           /* parameters.put("latitude","33.5169");
            parameters.put("longitude","73.8986");*/
            parameters.put("latitude", mGlobal.getLocation().split(",")[0]);
            parameters.put("longitude",mGlobal.getLocation().split(",")[1]);
            if (mGlobal.GetActiveUser()!=null)
                parameters.put("user_id",mGlobal.GetActiveUser().getUser_id());
        }

        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters,Constants.Search_gym);
        Log.d("Search params", parameters.toString());
        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        JSONObject jsonObject=null;
        try {
            if (!result.isEmpty()||result!=null)
            {
                jsonObject = new JSONObject(result);
                ArrayList<Gym> gymList;
                if (jsonObject.getString("status").equals("true")) {
                    Gson gson = new Gson();
                    Gym[] gym = gson.fromJson(jsonObject.getString("response"), Gym[].class);
                    gymList = new ArrayList<>(Arrays.asList(gym));
                    mDiscoverFragment.API_Result(gymList, jsonObject.getBoolean("near_home_gym"));
                }
                else
                {
                    mActivity.mViewProgressBar.setVisibility(View.GONE);
                    mDiscoverFragment.No_Gym_Found();
                }

            }
            else
            {
                MenuActivity.mViewProgressBar.setVisibility(View.GONE);
            }
        }
        catch (Exception e)
        { if (!Utility.isConnected(mContext))
        {
            mGlobal.ShoeDialog(mActivity);
            mDiscoverFragment.No_Gym_Found();
        }
            else {
            Toast.makeText(mActivity, "bad Internet connection.Please try again.", Toast.LENGTH_SHORT).show();
            MenuActivity.mViewProgressBar.setVisibility(View.GONE);
            mDiscoverFragment.No_Gym_Found();
        }
        }

    }
}
