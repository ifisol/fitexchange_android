package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Fragments.CodePreviousFragment;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class PreviousGymVisitTask extends AsyncTask<String, Void, String> {
    MenuActivity mActivity;
    Context mContext;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    CodePreviousFragment fragmentt;
    HashMap<String, String> parameters;

    public PreviousGymVisitTask(MenuActivity activity,CodePreviousFragment fragmenttt, HashMap<String, String> params) {
        mActivity = activity;
        mContext = activity;
        fragmentt=fragmenttt;
        mGlobal =(GlobalActivity) activity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        parameters=params;
 //      MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.PREVIOUS_VISIT);
        Log.d("Login Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
       MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            if (jsonObject.getString("status").equals("true"))
            {
                Gson gson=new Gson();
                Gym[] gyms= gson.fromJson(jsonObject.getString("response"), Gym[].class);
                ArrayList<Gym> gymArrayList = new ArrayList<>(Arrays.asList(gyms));
                fragmentt.API_RESULT(gymArrayList);
            }
            else
            {
                fragmentt.API_RESULT(null);
            }
        } catch (JSONException e) {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }
    }
}
