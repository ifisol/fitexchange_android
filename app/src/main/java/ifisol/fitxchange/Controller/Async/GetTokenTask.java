package ifisol.fitxchange.Controller.Async;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Subscription_Plans;
import ifisol.fitxchange.Model.Users;
import ifisol.fitxchange.View.Activities.LoginActivity;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Fragments.SettingsFragment;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class GetTokenTask extends AsyncTask<String, Void, String> {
    SettingsFragment mFragment;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    Subscription_Plans obj;
    Activity mActivity;


    public GetTokenTask(SettingsFragment fragment, Subscription_Plans object) {
        mFragment = fragment;
        obj=object;
        mActivity=mFragment.getActivity();
        mGlobal = (GlobalActivity) mFragment.getActivity().getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(new HashMap<String, String>(), Constants.Get_Token);

        Log.d("GetToken Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
        if (!result.isEmpty()||result!=null) {

                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {

                    String token = jsonObject.getString("response");
                    mFragment.InitializeBraintree(token, obj);

                }
            }


            } catch (Exception e) {
            Toast.makeText(mActivity,"Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();

        }

    }
}
