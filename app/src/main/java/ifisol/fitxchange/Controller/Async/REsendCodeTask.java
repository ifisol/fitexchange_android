package ifisol.fitxchange.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.View.Activities.VerificationCodeActivity;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class REsendCodeTask extends AsyncTask<String, Void, String> {
    VerificationCodeActivity mActivity;
    Context mContext;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameter;
    public REsendCodeTask(VerificationCodeActivity activity, HashMap<String, String> params) {
        mActivity = activity;
        mContext = activity;
        mGlobal = (GlobalActivity) activity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
         mActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        parameter=params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameter, Constants.RESEND_CODE);
        Log.d("Login Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            if (jsonObject.getString("status").equals("true"))
            {
                mActivity.API_RESPONSE();
            }
            else
            {
                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e)
        {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }
    }
}
