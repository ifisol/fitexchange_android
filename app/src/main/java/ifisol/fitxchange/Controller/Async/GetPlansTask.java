package ifisol.fitxchange.Controller.Async;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.Model.Subscription_Plans;
import ifisol.fitxchange.R;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Activities.RegisterActivity;
import ifisol.fitxchange.View.Fragments.SettingsFragment;
import ifisol.fitxchange.json.JSONArrayParser;

/**
 * Created by MEH on 7/27/2016.
 */
public class GetPlansTask extends AsyncTask<String, Void, String> {

    private String resp;
    Activity mActivity;
    SettingsFragment mFragment;

    JSONArrayParser jsonArrayParser;

    public GetPlansTask(SettingsFragment fragment) {

        mFragment=fragment;
        mActivity = mFragment.getActivity();
        jsonArrayParser = new JSONArrayParser();

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... prms) {

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",prms[0]);
        resp = this.jsonArrayParser.getJSONFromUrl_POST(params, Constants.Get_plans);
        Log.d("Plans.....", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {

        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString("status").equals("true"))
            {
                String onTrial = "no" ;
                String expiry = "";


                if (jsonObject.has("on_trial"))
                    onTrial = jsonObject.getString("on_trial");

                if (onTrial.equals("yes"))
                     expiry = jsonObject.getString("trial_end_date");

                Gson gson=new Gson();
                Subscription_Plans[] plans= gson.fromJson(jsonObject.getString("response"), Subscription_Plans[].class);
                ArrayList<Subscription_Plans> plansList = new ArrayList<>(Arrays.asList(plans));
                mFragment.PlansApiRes(plansList, onTrial, expiry);
            }
            else
            {
                mFragment.PlansApiRes(null,"","");
            }
        } catch (JSONException e) {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
            mFragment.PlansApiRes(null,"","");
        }
    }
}
