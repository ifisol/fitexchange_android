package ifisol.fitxchange.View.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ifisol.fitxchange.Controller.Async.TrainNowTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.Model.Services;
import ifisol.fitxchange.Model.Timings;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Activities.RegisterActivity;
//import ifisol.fitxchange.Controller.Async.GymDetailTask;


public class GymDetailFragment extends Fragment implements View.OnClickListener {
    GoogleMap map;
    RecyclerView rCyclerViewer;
    ImageView imgFilter, img_Phone;
    ImageView img_manu, img_back;
    View mView, mViewBottom;
    Button btRegisterAndTrain;
    GymDetailAdopter gdAdopter;
    TextView haeder, tvOpenningHr, tvOpenningMin, tvClosingHr, tvClosingMin, tvNameOfGym, tv_Location, tv_Adress;
    TextView tvOpenTime;
    TextView tvCloseTime;
    public TextView mtvSun, mtvMon, mtvTeu, mtvWed, mtvThur, mtvFri, mtvSat;
    GridLayoutManager gridLayoutManager;
    ArrayList<TextView> mArrayTextView;
    TextView tvAMfirst;
    GymDetailFragment mFragment;
    Context mContext;
    Gym gym;
    Activity mActivity;
    public ArrayList<Services> mArray;
    ArrayList<Timings> mAry;
    GlobalActivity mGlobal;
    MenuActivity menuActivity;
    ImageView gymlogo;
    TextView tvPublicHolydayWebsite;
    View vClose;
    View vOpen;
    String isfragment;
    View mDaysView;
    View mTimingsView;
    View mAddress;
    View view;
    boolean isNearHomeGym;
    boolean hasClosed;
    boolean isNeverAskAgainCall = false;
    Marker mapMarker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view == null) view = inflater.inflate(R.layout.gym_detail, container, false);
        rCyclerViewer = (RecyclerView) view.findViewById(R.id.recycler_jym_detail);
        mFragment = this;
        return Init(view);
    }

    private View Init(View view) {

        mContext = getActivity();
        mActivity = getActivity();
        menuActivity = (MenuActivity) getActivity();
        mGlobal = (GlobalActivity) mContext.getApplicationContext();
        imgFilter = MenuActivity.imgFilter;
        btRegisterAndTrain = (Button) view.findViewById(R.id.bt_register_and_train);
        tvNameOfGym = (TextView) view.findViewById(R.id.tv_name_of_jym);
        img_Phone = (ImageView) view.findViewById(R.id.img_phone);
        tvPublicHolydayWebsite = (TextView) view.findViewById(R.id.tv_website_public);
        vClose = view.findViewById(R.id.view_close);
        vOpen = view.findViewById(R.id.view_open);
        tvCloseTime = (TextView) view.findViewById(R.id.tv_close_time);
        tvOpenTime = (TextView) view.findViewById(R.id.tv_open_time);
        tv_Location = (TextView) view.findViewById(R.id.tv_location_distance);
        tv_Adress = (TextView) view.findViewById(R.id.tv_adress);
        mAddress = view.findViewById(R.id.view_adress);
        gymlogo = (ImageView) view.findViewById(R.id.img_gymlogo);
        //   tv_Phone = (TextView) view.findViewById(R.id.tv_phon_no);
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                map = googleMap;
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                map.getUiSettings().setRotateGesturesEnabled(true);
                ShowLocation();
            }
        });
        mtvSun = (TextView) view.findViewById(R.id.tv_sunday);
        mtvMon = (TextView) view.findViewById(R.id.tv_monday);
        mtvTeu = (TextView) view.findViewById(R.id.tv_teusday);
        mtvWed = (TextView) view.findViewById(R.id.tv_wednesday);
        mtvThur = (TextView) view.findViewById(R.id.tv_thursday);
        mtvFri = (TextView) view.findViewById(R.id.tv_friday);
        mtvSat = (TextView) view.findViewById(R.id.tv_saturday);
        mDaysView = (View) view.findViewById(R.id.days_view);
        mTimingsView = (View) view.findViewById(R.id.view_timings);
        mView = view.findViewById(R.id.view_website);
        tvOpenningHr = (TextView) view.findViewById(R.id.tv_openning_hour);
        tvOpenningMin = (TextView) view.findViewById(R.id.tv_openning_min);
        tvClosingHr = (TextView) view.findViewById(R.id.tv_closing_hour);
        tvClosingMin = (TextView) view.findViewById(R.id.tv_closing_min);
        //relativeLayout=(RelativeLayout)view.findViewById(R.id.view_tranparent_top);
        btRegisterAndTrain.setOnClickListener(this);
        mView.setOnClickListener(this);
        mView.setOnClickListener(this);
        mtvSun.setOnClickListener(this);
        mtvMon.setOnClickListener(this);
        mtvTeu.setOnClickListener(this);
        mtvWed.setOnClickListener(this);
        mtvThur.setOnClickListener(this);
        mtvFri.setOnClickListener(this);
        mtvSat.setOnClickListener(this);
        img_Phone.setOnClickListener(this);
        mAddress.setOnClickListener(this);
        tvPublicHolydayWebsite.setOnClickListener(this);
        return view;
    }
    //This gym has split staffed hours. Please check the gym's website to confirm staffed hours and public holidays before selecting Train Now.

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        mArray = new ArrayList<>();
        this.mContext = getActivity();
        mGlobal = (GlobalActivity) mContext.getApplicationContext();
        if (mGlobal.GetActiveUser() != null) {
            btRegisterAndTrain.setText("Train Now");
        }
        else
            btRegisterAndTrain.setText("Register");

        Bundle bundl = getArguments();
        gym = (Gym) bundl.getSerializable("obj");
        isNearHomeGym = bundl.getBoolean("IS_HOME");
        if (gym.getIsSplitHours() != null && gym.getIsSplitHours().equalsIgnoreCase("true")) {
            tvPublicHolydayWebsite.setText("This gym has split staffed hours. Please check the gym's website to confirm staffed hours and public holidays before selecting Train Now.");
        } else {
            tvPublicHolydayWebsite.setText("Check website for public holiday opening hours");
        }
        if (isNearHomeGym) MenuActivity.imgHome.setVisibility(View.VISIBLE);
        else MenuActivity.imgHome.setVisibility(View.GONE);

        /*String previous=(String)bundl.getSerializable("str");*/
        isfragment = bundl.getString("str");

        if (mGlobal.GetActiveUser() != null) {
            if (mGlobal.GetActiveUser().getGym_id().equals(gym.getGym_id())) {
                btRegisterAndTrain.setVisibility(View.GONE);
            }
            else
            {
                if (!isfragment.equals("")) {
                    if (isfragment.equals("previous") || gym.getWithin_range().equals("false")) {
                        btRegisterAndTrain.setVisibility(View.GONE);
                    } else
                    {

                        Calendar calendar = Calendar.getInstance();
                        String dayName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
                        Timings timings = null;
                        for (int i = 0; i < gym.getGym_timings().size(); i++) {
                            if (gym.getGym_timings().get(i).getDay().equalsIgnoreCase(dayName)) {
                                timings = gym.getGym_timings().get(i);
                                break;
                            }
                        }

                            String openingTime = timings.getOpen_timing();
                            String closingTime = timings.getClose_timing();
                            if (openingTime.equals("")||closingTime.equals(""))
                            {
                                btRegisterAndTrain.setVisibility(View.GONE);
                            }
                            else
                                btRegisterAndTrain.setVisibility(View.VISIBLE);

                    }

                }
            }
        }
        else
            btRegisterAndTrain.setVisibility(View.VISIBLE);







        String img = gym.getGym_logo();
        if (!img.equals("") && img != null) {
            Picasso.with(mContext).load(Constants.Img_URL + gym.getGym_logo()).error(R.drawable.ic_placholdr).into(gymlogo);
        }
        else
            gymlogo.setImageResource(R.drawable.ic_placholdr);


        tvNameOfGym.setText(gym.getGym_name());
        tv_Location.setText(gym.getGym_address());
        tv_Adress.setText(gym.getGym_address());
        mAry = gym.getGym_timings();
        mViewBottom = MenuActivity.mViewBottom;
        img_manu = MenuActivity.imgMenu;
        img_back = MenuActivity.img_back_gymdetail;
        img_back.setVisibility(View.VISIBLE);
        mViewBottom.setVisibility(View.GONE);
        img_back.setOnClickListener(this);
        mArrayTextView = new ArrayList<TextView>();
        mArrayTextView.add(mtvSun);
        mArrayTextView.add(mtvMon);
        mArrayTextView.add(mtvTeu);
        mArrayTextView.add(mtvWed);
        mArrayTextView.add(mtvThur);
        mArrayTextView.add(mtvFri);
        mArrayTextView.add(mtvSat);
        Calendar calendar = Calendar.getInstance();
        String dayName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());

   //     SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE");
  //      String dayName = dayFormat.format(calendar.getTime());

        Log.d("today is...............", dayName);


        if (dayName.equalsIgnoreCase("Monday")) {
            SetTimings(dayName, mtvMon);
        } else if (dayName.equalsIgnoreCase("Tuesday")) {
            SetTimings(dayName, mtvTeu);
        } else if (dayName.equalsIgnoreCase("Wednesday")) {
            SetTimings(dayName, mtvWed);
        } else if (dayName.equalsIgnoreCase("Thursday")) {
            SetTimings(dayName, mtvThur);
        } else if (dayName.equalsIgnoreCase("Friday")) {
            SetTimings(dayName, mtvFri);
        } else if (dayName.equalsIgnoreCase("Saturday")) {
            SetTimings(dayName, mtvSat);
        } else if (dayName.equalsIgnoreCase("Sunday")) {
            SetTimings(dayName, mtvSun);
        }
        haeder = MenuActivity.header;
        haeder.setText("Gym Detail");
        imgFilter.setVisibility(View.GONE);
        ArrayList<Services> mAarray = gym.getGym_features();
        gridLayoutManager = new GridLayoutManager(mContext, 2);
        rCyclerViewer.setHasFixedSize(true);
        rCyclerViewer.setLayoutManager(gridLayoutManager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mContext, R.dimen.dp_3);
        rCyclerViewer.addItemDecoration(itemDecoration);
        gdAdopter = new GymDetailAdopter(mAarray, mContext);
        rCyclerViewer.setAdapter(gdAdopter);


        hasClosedToday();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bt_register_and_train:
                if (mGlobal.GetActiveUser() != null) {
                    if (!Utility.isConnected(mContext)) {
                        mGlobal.ShoeDialog(getActivity());
                    } else {
                        if (mGlobal.GetActiveUser().getIs_subscribed().equals("no") && mGlobal.GetActiveUser().getOn_trial().equals("no")) {
                            Utility.ReplaceFragment(new SettingsFragment(), getActivity().getSupportFragmentManager());
                        } else {
                            if (isNearHomeGym)
                                Toast.makeText(mContext, "Unfortunately this gym falls in the 30km blackout. In other words it's too close to your home gym. Check out the Fit Exchange fair go policy and limitations on our website for more info.", Toast.LENGTH_LONG).show();
                            else {
                                if (mGlobal.GetActiveUser().getOn_contract().equals("yes")) {
                                    try {
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                        Date ExpDate = sdf.parse(mGlobal.GetActiveUser().getContract_expiry());
                                        if (new Date().after(ExpDate)) {
                                            ExpiredDialog(menuActivity);
                                        } else
                                        {
                                                 TrainNOW();

                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                else
                                    TrainNOW();


                            }

                        }
                    }
                    // Utility.ReplaceFragment(new CodesFragment(), getFragmentManager());
                    // MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
                } else {
                    Utility.OpenActivityPrevious(mContext, getActivity(), RegisterActivity.class);
                }
                break;
            case R.id.tv_sunday:
                SetTimings("sunday", mtvSun);
                break;
            case R.id.tv_monday:
                SetTimings("monday", mtvMon);
                break;
            case R.id.tv_teusday:
                SetTimings("tuesday", mtvTeu);
                break;
            case R.id.tv_wednesday:
                SetTimings("wednesday", mtvWed);
                break;
            case R.id.tv_thursday:
                SetTimings("thursday", mtvThur);
                break;
            case R.id.tv_friday:
                SetTimings("friday", mtvFri);
                break;
            case R.id.tv_saturday:
                SetTimings("saturday", mtvSat);
                break;
            case R.id.view_website:
                String url = gym.getGym_website();
                if (url != null && !url.equals("")) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(("http://" + url)));
                    startActivity(i);
                }
                break;
            case R.id.img_backk:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.img_phone:
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    CheckPermission();
                } else {
                    String contact_number = gym.getGym_phone();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + contact_number));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                break;
            case R.id.tv_website_public:
                String urll = gym.getGym_website();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(("http://" + urll)));
                startActivity(i);
                break;
            case R.id.view_adress:
                DirectionDialog();
                break;

        }
    }

    public static void ExpiredDialog(Activity mActivity) {

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.trial_expired_dialog);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        TextView btnClose = (TextView) dialog.findViewById(R.id.bt_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        dialog.show();
    }



    public void DirectionDialog() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_direction_alert);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
    //    lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btnNo = (Button) dialog.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                String uri = "http://maps.google.com/maps?f=d&hl=en&saddr="+mGlobal.getLocation().split(",")[0]+","+mGlobal.getLocation().split(",")[1]+"&daddr="+gym.getLatitude()+","+gym.getLongitude();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                mContext.startActivity(Intent.createChooser(intent, "Select an application"));

            }
        });
        dialog.show();

    }









    private void CheckPermission() {

        if (ContextCompat.checkSelfPermission(menuActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (isNeverAskAgainCall) {
                ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Phone permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
            } else mFragment.requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 7);
        } else {
            String contact_number = gym.getGym_phone();
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + contact_number));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    private void ShowPermissionWarning(final String retry, String desc) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_permission_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvHeading = (TextView) dialog.findViewById(R.id.tv_heading);
        TextView tvDesc = (TextView) dialog.findViewById(R.id.tv_desc);
        TextView tvRetry = (TextView) dialog.findViewById(R.id.tv_retry);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        tvHeading.setText("Phone Permission Required");
        tvDesc.setText(desc);
        tvRetry.setText(retry);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (retry.equalsIgnoreCase("retry")) {
                    CheckPermission();
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void SetTimings(String day, TextView tv) {

        for (int i = 0; i < gym.getGym_timings().size(); i++) {
            if (gym.getGym_timings().get(i).getDay().equalsIgnoreCase(day)) {
                String openingTime = gym.getGym_timings().get(i).getOpen_timing();
                String closingTime = gym.getGym_timings().get(i).getClose_timing();
                String openining_Time_Meridium = "";
                String closing_Time_Meridium = "";
                if (openingTime != null && !openingTime.equals("") || closingTime != null && !closingTime.equals("")) {
                    String[] splitStr = openingTime.split("\\s+");
                    String[] splitStrr = closingTime.split("\\s+");
                    openining_Time_Meridium = splitStr[1];
                    closing_Time_Meridium = splitStrr[1];
                }
                SelectedDay(tv);
                if (openingTime.equals("") || closingTime.equals("")) {
                    vClose.setVisibility(View.VISIBLE);
                    vOpen.setVisibility(View.GONE);

                } else {

                    vClose.setVisibility(View.GONE);
                    vOpen.setVisibility(View.VISIBLE);
                    SetTime(openingTime, closingTime, openining_Time_Meridium, closing_Time_Meridium);

                }
            }
        }
    }

    private void SelectedDay(TextView view) {

        for (int i = 0; i < mArrayTextView.size(); i++) {
            mArrayTextView.get(i).setBackground(mContext.getResources().getDrawable(R.drawable.day_uncheck));
            mArrayTextView.get(i).setTextColor(getResources().getColor(R.color.transparent_grey));

        }
        view.setBackground(mContext.getResources().getDrawable(R.drawable.day_check));
        view.setTextColor(getResources().getColor(R.color.white));

    }

    private void ShowLocation() {



        try {
                double latti = Double.parseDouble(gym.getLatitude());
                double longi = Double.parseDouble(gym.getLongitude());

                BitmapDescriptor icon = null;
                if (gym.getWithin_range().equals("true"))
                    icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_range_in);
                else
                    icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_range_out);

                mapMarker =  map.addMarker(new MarkerOptions().position(new LatLng(latti, longi)).visible(true).icon(icon).title(gym.getGym_address()));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latti, longi), 15));

                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        DirectionDialog();
                        return false;
                    }
                });


        } catch (Exception e) {
            e.printStackTrace();
        }
        // }
    }

    private void TrainNOW() {

        if (hasClosed)
        {
            Toast.makeText(mContext, "Gym is now closed", Toast.LENGTH_SHORT).show();
            return;
        }
        if (gym.getWithin_range().equals("no"))
        {
            Toast.makeText(mContext, "Sorry! You cannot Train now in this Gym. This falls out off 30km radius", Toast.LENGTH_SHORT).show();
            return;
        }




        HashMap<String, String> param = new HashMap<>();
        String USER_ID = mGlobal.GetActiveUser().getUser_id();
        param.put("user_id", USER_ID);
        param.put("gym_id", gym.getGym_id());
        new TrainNowTask(menuActivity, mFragment, param).execute();
    }

    public void TRAIN_NOW_API_RESULT(String gymCode) {

        mGlobal.SaveCode(gymCode);
        mGlobal.SaveGym(gym);
        mGlobal.saveShared(false);
        Utility.ReplaceFragment(new CodesFragment(), getFragmentManager());
    }

    private void SetTime(String openingTime, String closingTime, String opening_time_meridium, String closing_time_meridium) {

        String tim = openingTime;
        String[] hour = tim.split(":");
        String first = hour[0];
        String sec = hour[1];
        String[] second = sec.split("\\s+");
        String min = second[0];
        String meridium = second[1];
        String time = closingTime;
        String[] houre = time.split(":");
        String firste = houre[0];
        String sece = houre[1];
        String[] seconde = sece.split("\\s+");
        String mine = seconde[0];
        String meridiume = seconde[1];
        tvOpenTime.setText(opening_time_meridium);
        tvCloseTime.setText(closing_time_meridium);
        tvOpenningHr.setText(first);
        tvOpenningMin.setText(min);
        tvClosingMin.setText(firste);
        tvClosingHr.setText(mine);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 7:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    String contact_number = gym.getGym_phone();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + contact_number));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    if (!shouldShowRequestPermissionRationale(permissions[0])) {
                        isNeverAskAgainCall = true;
                        //ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    } else {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }
                break;

        }
    }

    private void hasClosedToday() {

        Calendar calendar = Calendar.getInstance();
        String dayName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());

     //   SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE");
   //     String dayName = dayFormat.format(calendar.getTime());

        for (int i = 0; i < gym.getGym_timings().size(); i++) {
            if (gym.getGym_timings().get(i).getDay().equalsIgnoreCase(dayName)) {
                try {

                    DateFormat df = new SimpleDateFormat("hh:mm a");
                    Date closingTime = df.parse(gym.getGym_timings().get(i).getClose_timing());
                    Date startTime = df.parse(gym.getGym_timings().get(i).getOpen_timing());
                    String current = df.format(new Date());
                    Date currentTime = df.parse(current);

                    Log.d("Day name > > >", dayName);
                    Log.d("Opening time > > >", gym.getGym_timings().get(i).getOpen_timing());
                    Log.d("Closing time > > >", gym.getGym_timings().get(i).getClose_timing());
                    Log.d("Current time > > >", current);


                    if (closingTime.before(currentTime) || startTime.after(currentTime))
                    {
                       hasClosed = true;
                        Log.d("closed > > > > > > >", "Gym is closed at now");
                    }

                    else
                    {
                        Log.d("Opened > > > > > > >", "Gym is opened at now");
                        hasClosed = false;
                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();

                }


            }

        }

    }
}
