package ifisol.fitxchange.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.HashMap;

import ifisol.fitxchange.Controller.Adapters.PreviousAdopter;
import ifisol.fitxchange.Controller.Async.PreviousGymVisitTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.MenuActivity;

/**
 * Created by MEH on 6/24/2016.
 */
public class CodePreviousFragment extends Fragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    ListView lvPrevious;
    Context context;
    CodePreviousFragment prviousFragment;
    PreviousAdopter mAdopter;
    MenuActivity mMenueActivity;
    GlobalActivity mGlobal;
    View viewe;
    private SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_codes_previous, container, false);
        return Init(view);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchGyms();
        lvPrevious.setOnItemClickListener(this);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange_ftx));
        swipeRefreshLayout.setOnRefreshListener(this);
    }




    private View Init(View view) {

        mMenueActivity=(MenuActivity)getActivity();
        context=getActivity();
        prviousFragment=this;
        mGlobal=(GlobalActivity)context.getApplicationContext();

        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        lvPrevious=(ListView)view.findViewById(R.id.list_previous);
        viewe= view.findViewById(R.id.view_no_gym_found);
        return view;
    }





    public void API_RESULT(ArrayList<Gym> gymArrayList) {

        if (swipeRefreshLayout.isRefreshing())
             swipeRefreshLayout.setRefreshing(false);
        if (gymArrayList == null || gymArrayList.size() == 0) {
            viewe.setVisibility(View.VISIBLE);
            lvPrevious.setVisibility(View.GONE);

        } else {
                viewe.setVisibility(View.GONE);
                lvPrevious.setVisibility(View.VISIBLE);
                mAdopter = new PreviousAdopter(gymArrayList, context);
                lvPrevious.setAdapter(mAdopter);
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        Gym gym=(Gym)mAdopter.getItem(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("obj",gym);
        bundle.putString("str", "previous");
        Utility.ReplaceFragment(new GymDetailFragment(),getActivity().getSupportFragmentManager(),bundle);
    }

    private void fetchGyms()
    {
        String User_idd=mGlobal.GetActiveUser().getUser_id();
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", User_idd);
        new PreviousGymVisitTask(mMenueActivity,prviousFragment,param).execute();
    }



    @Override
    public void onRefresh() {
        fetchGyms();
    }
}
