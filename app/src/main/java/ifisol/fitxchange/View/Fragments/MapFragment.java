package ifisol.fitxchange.View.Fragments;

import android.content.Context;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.MenuActivity;

/**
 * Created by MEH on 8/30/2016.
 */
public class MapFragment extends Fragment{
    Context mContext;
    private GoogleMap map;
    ArrayList<Gym> array;
    List<Address> addresses = null;
    Marker mp;
    Marker mpp;
    Marker lastOpenned = null;
    GlobalActivity mGlobal;
    ArrayList<Marker>markers;
    Circle mCircle;
    String gymName;
    String gmyImage;

    ArrayList<ImageView> mIVlist;
    ImageView mImg1;
    ImageView mImg2;
    ImageView mImg3;
    ImageView mImg4;
    ImageView mImg5;
    ImageView mImg6;
    ImageView mImg7;
    ImageView mImg8;
    ImageView mImg9;
    ImageView mImg10;

    TextView mTvViewAllGyms;

    boolean isNearHomeGym;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && !bundle.equals("")) {
            array = (ArrayList<Gym>) bundle.getSerializable("array");
            isNearHomeGym = bundle.getBoolean("IS_HOME");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        mTvViewAllGyms = (TextView) view.findViewById(R.id.tv_view_all_gyms);
        mTvViewAllGyms.setVisibility(View.GONE); // WE REMOVED THIS FEATURE
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext=getActivity();
        MenuActivity.imgFilter.setVisibility(View.GONE);
       // MenuActivity.imgHome.setVisibility(View.VISIBLE);
        mGlobal=(GlobalActivity)mContext.getApplicationContext();
        DiscoverMainFragment.viewlist.setVisibility(View.GONE);
        DiscoverMainFragment.viewMap.setVisibility(View.VISIBLE);
        markers=new ArrayList<>();


            if (isNearHomeGym)
                MenuActivity.imgHome.setVisibility(View.VISIBLE);
            else
                MenuActivity.imgHome.setVisibility(View.GONE);



        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mape)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                map = googleMap;
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                map.getUiSettings().setMyLocationButtonEnabled(false);
              /*  LocationClass loca = mGlobal.getmLocation();
                String ss = loca.getLocc();*/
                String ss=mGlobal.getLocation();
                String[] location = ss.split(",");
                String lat = location[0];
                String lng = location[1];
                double latt = Double.parseDouble(lat);
                double lnng = Double.parseDouble(lng);
                //    mCircle = map.addCircle(new CircleOptions().center(new LatLng(latt, lnng)).radius(3000).strokeColor(getActivity().getResources().getColor(R.color.orange_ftx_80)).fillColor(getActivity().getResources().getColor(R.color.orange_ftx50)));

                map.addMarker(new MarkerOptions().position(new LatLng(latt, lnng)).visible(true).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title("Me"));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latt, lnng), 13));


                if (array != null && array.size() > 0) {
                    try {
                        for (int i = 0; i < array.size(); i++) {
                            Double latitude = Double.parseDouble(array.get(i).getLatitude());
                            Double longitude = Double.parseDouble(array.get(i).getLongitude());
                            if (!latitude.equals("")&&!latitude.equals(null)&&!longitude.equals(null)&&!longitude.equals(""))
                            {
                                BitmapDescriptor icon = null;
                                if (array.get(i).getWithin_range().equals("true"))
                                    icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_range_in);
                                else
                                    icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_range_out);
                                mp = map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).visible(true).icon(icon));
                                markers.add(mp);
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter()

            {
                @Override
                public View getInfoWindow (Marker marker){

                return null;
            }

                @Override
                public View getInfoContents (Marker marker){

                View v = null;
                int id = Integer.valueOf(markers.indexOf(marker));
                if (id != -1) {
                    v = getActivity().getLayoutInflater().inflate(R.layout.marker_maplayout, null);
                    TextView gymname = (TextView) v.findViewById(R.id.tv_jym_name);
                    ImageView gymLogo = (ImageView) v.findViewById(R.id.jymprofile_image);

                    gymName = array.get(id).getGym_name();
                    gymname.setText(gymName);
                    gmyImage = array.get(id).getGym_logo();
                    if (!gymLogo.equals("") && gymLogo != null) {
                        Picasso.with(mContext).load(Constants.Img_URL + gmyImage).error(R.drawable.ic_placholdr).into(gymLogo);
                    }
                    else
                        gymLogo.setImageResource(R.drawable.ic_placholdr);
                    mIVlist = new ArrayList<>();
                    mImg1 = (ImageView) v.findViewById(R.id.img_1);
                    mImg2 = (ImageView) v.findViewById(R.id.img_2);
                    mImg3 = (ImageView) v.findViewById(R.id.img_3);
                    mImg4 = (ImageView) v.findViewById(R.id.img_4);
                    mImg5 = (ImageView) v.findViewById(R.id.img_5);
                    mImg6 = (ImageView) v.findViewById(R.id.img_6);
                    mImg7 = (ImageView) v.findViewById(R.id.img_7);
                    mImg8 = (ImageView) v.findViewById(R.id.img_8);
                    mImg9 = (ImageView) v.findViewById(R.id.img_9);
                    mImg10 = (ImageView) v.findViewById(R.id.img_10);
                    mImg1.setTag("boxing.png");
                    mImg2.setTag("cardio.png");
                    mImg3.setTag("free_weights.png");
                    mImg4.setTag("machine_weights.png");
                    mImg5.setTag("pool.png");
                    mImg6.setTag("wom_only.png");
                    mImg7.setTag("x_fit.png");
                    mImg8.setTag("yoga.png");
                    mImg9.setTag("other.png");
                    mImg10.setTag("group_fitness.png");
                    mImg1.setVisibility(View.GONE);
                    mImg2.setVisibility(View.GONE);
                    mImg3.setVisibility(View.GONE);
                    mImg4.setVisibility(View.GONE);
                    mImg5.setVisibility(View.GONE);
                    mImg6.setVisibility(View.GONE);
                    mImg7.setVisibility(View.GONE);
                    mImg8.setVisibility(View.GONE);
                    mImg9.setVisibility(View.GONE);
                    mImg10.setVisibility(View.GONE);
                    mIVlist.add(mImg1);
                    mIVlist.add(mImg2);
                    mIVlist.add(mImg3);
                    mIVlist.add(mImg4);
                    mIVlist.add(mImg5);
                    mIVlist.add(mImg6);
                    mIVlist.add(mImg7);
                    mIVlist.add(mImg8);
                    mIVlist.add(mImg9);
                    mIVlist.add(mImg10);
                    for (int i = 0; i < array.get(id).getGym_features().size(); i++) {
                        String img = array.get(id).getGym_features().get(i).getFeature_img();
                        for (ImageView obj : mIVlist) {
                            if (img.equalsIgnoreCase(obj.getTag().toString())) {
                                obj.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
                // array.indexOf(id);
                return v;
            }
            }

            );
            map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener()

            {
                @Override
                public void onInfoWindowClick (Marker marker) {
                    int id = Integer.valueOf(markers.indexOf(marker));

                    if (id==-1)
                    {

                    } else {
                        Gym gym = array.get(id);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("obj", gym);
                        bundle.putString("str", "map");
                        bundle.putBoolean("IS_HOME", isNearHomeGym);
                        Utility.ReplaceFragment(new GymDetailFragment(), getFragmentManager(), bundle);
                    }
                }
            }

            );

            // map.getUiSettings().setRotateGesturesEnabled(true);

            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()

            {
                @Override
                public boolean onMarkerClick (Marker marker){
                if (marker.equals(-1)) {

                } else {
                    if (marker.equals(mpp)) {
                        marker.hideInfoWindow();
                        return true;
                    }
                    if (lastOpenned != null) {
                        lastOpenned.hideInfoWindow();
                        if (lastOpenned.equals(marker)) {
                            lastOpenned = null;
                            return true;
                        }
                    }
                    final int dX = getResources().getDimensionPixelSize(R.dimen.map_dx);
                    final int dY = getResources().getDimensionPixelSize(R.dimen.map_dy);
                    final Projection projection = map.getProjection();
                    final Point markerPoint = projection.toScreenLocation(marker.getPosition());
                    markerPoint.offset(dX, dY);
                    final LatLng newLatLng = projection.fromScreenLocation(markerPoint);
                    map.animateCamera(CameraUpdateFactory.newLatLng(newLatLng));
                    marker.showInfoWindow();
                    lastOpenned = marker;

                }
                return true;
            }
            }

            );

            map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener()

            {
                @Override
                public void onMyLocationChange (Location location){
                if (mpp != null) {
                    mpp.remove();
                }
                if (mCircle != null) {
                    mCircle.remove();
                }

                //map.clear();
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                        /*mCircle = map.addCircle(new CircleOptions()
                                .center(new LatLng(latitude, longitude))
                                .radius(3000)
                                .strokeColor(getActivity().getResources().getColor(R.color.orange_ftx_80))
                                .fillColor(getActivity().getResources().getColor(R.color.orange_ftx50)));*/

                mpp = map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));
                mpp.setVisible(false);
                // markers.add(mpp);
                // markers.add(mp);
            }
            }

            );
        }
    });



       // markers.add(mpp);


        mTvViewAllGyms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
                Utility.ReplaceFragment(new ViewAllGymFragment(), getFragmentManager());

            /*    String url = "http://fitexchange.com.au/gym_locator/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);*/

            }
        });


    }


   /* @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;

    }*/

}
