package ifisol.fitxchange.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import ifisol.fitxchange.Controller.Adapters.CodePagerAdopter;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.R;
import ifisol.fitxchange.View.Activities.MenuActivity;

/**
 * Created by MEH on 6/24/2016.
 */
public class CodesFragment extends Fragment {
    ViewPager vPager;
    CodePagerAdopter pAdopter;
    public static ProgressBar mProg;
    ImageView imgFilter;
    TabLayout tabLayout;
    Context   context;
    MenuActivity  mMenueActivity;
    GlobalActivity mGlobal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_codes, container, false);
        return Init(view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context=getActivity();
        mGlobal=(GlobalActivity)context.getApplicationContext();

        /*final Handler handl=new Handler();
        handl.postDelayed(new Runnable() {
            @Override
            public void run() {
                ShareDialog();
            }

        }, 2000);*/
        MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        MenuActivity.header.setText("Code");
        MenuActivity menuActivity=(MenuActivity)getActivity();
        menuActivity.imgInfo.setVisibility(View.GONE);
        menuActivity.UpDate("code");
        MenuActivity.img_back_gymdetail.setVisibility(View.GONE);
        MenuActivity.imgHome.setVisibility(View.GONE);
        MenuActivity.mViewBottom.setVisibility(View.VISIBLE);

        pAdopter=new CodePagerAdopter(getChildFragmentManager());
        vPager.setAdapter(pAdopter);
        tabLayout.setupWithViewPager(vPager);
        imgFilter.setVisibility(View.GONE);
        //tabLayout.setTabsFromPagerAdapter(pAdopter);
        vPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                      // mProg.setVisibility(View.GONE);
                        break;
                    case 1:

                        break;
                }

            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void ShareDialog() {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_share_dialog);
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            window.setGravity(Gravity.CENTER | Gravity.CENTER);
            lp.copyFrom(window.getAttributes());

            //This makes the dialog take up the full width
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            Button btCancel=(Button)dialog.findViewById(R.id.bt_cancell);
           // ImageView canCel = (ImageView) dialog.findViewById(R.id.img_cancel);
           btCancel.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   dialog.dismiss();
               }
           });
            dialog.show();
        }

    private View Init(View view) {
        vPager       =     (ViewPager)view.findViewById(R.id.viewPager);
        tabLayout    =     (TabLayout)view.findViewById(R.id.tab_layout);
        mProg=(ProgressBar)view.findViewById(R.id.progress_bar);
        imgFilter= MenuActivity.imgFilter;
        mMenueActivity=(MenuActivity)getActivity();

        return view;
    }

}
