package ifisol.fitxchange.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ifisol.fitxchange.R;
import ifisol.fitxchange.View.Activities.MenuActivity;

/**
 * Created by MEH on 11/22/2016.
 */
public class ViewAllGymFragment extends Fragment implements View.OnClickListener {
    WebView wV_browser;
    Context mContext;
    MenuActivity mMenueActivity;
    String url = "http://fitexchange.com.au/gym/members/gym_locator";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_gyms, container, false);
        wV_browser = (WebView) view.findViewById(R.id.wv_browser);
        wV_browser.getSettings().setJavaScriptEnabled(true);
        wV_browser.getSettings().setLoadWithOverviewMode(true);
        wV_browser.getSettings().setUseWideViewPort(true);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMenueActivity = (MenuActivity) getActivity();
        MenuActivity.header.setText("All Gyms");
        MenuActivity.imgHome.setVisibility(View.GONE);
        MenuActivity.img_back_gymdetail.setVisibility(View.VISIBLE);
        MenuActivity.img_back_gymdetail.setOnClickListener(this);
        wV_browser.setWebViewClient(new MyWebViewClient());
        wV_browser.getSettings().setJavaScriptEnabled(true);
        if (!url.equals("")) {
            wV_browser.loadUrl(url);
        } else {
            MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        FragmentManager fm = getActivity()
                .getSupportFragmentManager();
        fm.popBackStack();
       // MenuActivity.imgHome.setVisibility(View.VISIBLE);

    }

    public class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            MenuActivity.mViewProgressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }
    }

}
