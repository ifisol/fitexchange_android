package ifisol.fitxchange.View.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ifisol.fitxchange.Controller.Async.Get_Gym_info_Task;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.Model.Services;
import ifisol.fitxchange.Model.Timings;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.MenuActivity;
//import ifisol.fitxchange.Controller.Async.GymDetailTask;


public class OwnGymDetailFragment extends Fragment implements View.OnClickListener {
    Context context;
    GoogleMap map;
    RecyclerView rCyclerViewer;
    ImageView imgFilter, img_Phone;
    View mView, mViewBottom;

    GymDetailAdopter gdAdopter;
    TextView haeder, tvOpenningHr, tvOpenningMin, tvClosingHr, tvClosingMin, tvNameOfGym,
            tv_Location, tv_Adress;
    TextView tvOpenTime;
    TextView tvCloseTime;
    public TextView mtvSun, mtvMon, mtvTeu, mtvWed, mtvThur, mtvFri, mtvSat;
    GridLayoutManager gridLayoutManager;
    ArrayList<TextView> mArrayTextView;
    OwnGymDetailFragment mFragment;
    Context mContext;
    Gym gym;
    public ArrayList<Services> mArray;
    ArrayList<Timings> mAry;
    GlobalActivity mGlobal;
    MenuActivity menuActivity;
    ImageView gymlogo;
    View vClose;
    View vOpen;
    View mDaysView;
    View mTimingsView;
    View view;
    View mAddress;
    boolean isNeverAskAgainCall = false;
    TextView mTvContractStart;
    TextView mTvContractEnd;
    View     mViewContract;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.own_gym_detail, container, false);
        rCyclerViewer = (RecyclerView) view.findViewById(R.id.recycler_jym_detail);
        mFragment = this;
        return Init(view);
    }


    private View Init(View view) {
        mContext = getActivity();
        menuActivity = (MenuActivity) getActivity();
        mGlobal = (GlobalActivity) mContext.getApplicationContext();

        MenuActivity.header.setText("Own Gym Detail");

        imgFilter = MenuActivity.imgFilter;
        tvNameOfGym = (TextView) view.findViewById(R.id.tv_name_of_jym);
        img_Phone = (ImageView) view.findViewById(R.id.img_phone);

        mTvContractStart = (TextView) view.findViewById(R.id.tv_contract_start_date);
        mTvContractEnd = (TextView) view.findViewById(R.id.tv_contract_end_date);
        mViewContract = view.findViewById(R.id.view_contract_exp);

        vClose = view.findViewById(R.id.view_close);
        vOpen = view.findViewById(R.id.view_open);
        tvCloseTime = (TextView) view.findViewById(R.id.tv_close_time);
        tvOpenTime = (TextView) view.findViewById(R.id.tv_open_time);
        tv_Location = (TextView) view.findViewById(R.id.tv_location_distance);
        tv_Adress = (TextView) view.findViewById(R.id.tv_adress);
        gymlogo = (ImageView) view.findViewById(R.id.img_gymlogo);
        //   tv_Phone = (TextView) view.findViewById(R.id.tv_phon_no);
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                map.getUiSettings().setRotateGesturesEnabled(true);

            }
        });
        mtvSun = (TextView) view.findViewById(R.id.tv_sunday);
        mtvMon = (TextView) view.findViewById(R.id.tv_monday);
        mtvTeu = (TextView) view.findViewById(R.id.tv_teusday);
        mtvWed = (TextView) view.findViewById(R.id.tv_wednesday);
        mtvThur = (TextView) view.findViewById(R.id.tv_thursday);
        mtvFri = (TextView) view.findViewById(R.id.tv_friday);
        mtvSat = (TextView) view.findViewById(R.id.tv_saturday);
        mDaysView = (View) view.findViewById(R.id.days_view);
        mTimingsView = (View) view.findViewById(R.id.view_timings);
        mAddress = view.findViewById(R.id.view_adress);
        mView = view.findViewById(R.id.view_website);
        tvOpenningHr = (TextView) view.findViewById(R.id.tv_openning_hour);
        tvOpenningMin = (TextView) view.findViewById(R.id.tv_openning_min);
        tvClosingHr = (TextView) view.findViewById(R.id.tv_closing_hour);
        tvClosingMin = (TextView) view.findViewById(R.id.tv_closing_min);
        //relativeLayout=(RelativeLayout)view.findViewById(R.id.view_tranparent_top);
        mView.setOnClickListener(this);
        mtvSun.setOnClickListener(this);
        mtvMon.setOnClickListener(this);
        mtvTeu.setOnClickListener(this);
        mtvWed.setOnClickListener(this);
        mtvThur.setOnClickListener(this);
        mtvFri.setOnClickListener(this);
        mtvSat.setOnClickListener(this);
        img_Phone.setOnClickListener(this);

        return view;
    }

    //This gym has split staffed hours. Please check the gym's website to confirm staffed hours and public holidays before selecting Train Now.

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        MenuActivity.imgSettings.setVisibility(View.GONE);
        MenuActivity.imgInfo.setVisibility(View.GONE);

        mArray = new ArrayList<>();
        this.context = getActivity();
        mGlobal = (GlobalActivity) context.getApplicationContext();

        new Get_Gym_info_Task(menuActivity,this).execute(mGlobal.GetActiveUser().getUser_id(), mGlobal.GetActiveUser().getGym_id());



    }

    public void InitData(Gym gm)
    {
        gym = gm;
        String img = gym.getGym_logo();
        if (!img.equals("") && img != null) {
            Picasso.with(mContext)
                    .load(Constants.Img_URL + img)
                    .error(R.drawable.ic_placholdr)
                    .into(gymlogo);
        }
        else
            gymlogo.setImageResource(R.drawable.ic_placholdr);

        if (gym.getOn_contract().equals("yes"))
        {

            mViewContract.setVisibility(View.VISIBLE);
            mTvContractStart.setText(gym.getContract_start());
            mTvContractEnd.setText(gym.getContract_expiry());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date ExpDate = sdf.parse(gym.getContract_expiry());
                if (new Date().after(ExpDate)) {
                    ExpiredDialog(menuActivity);
                    mTvContractEnd.setTextColor(menuActivity.getResources().getColor(R.color.red));
                }
                else
                    mTvContractEnd.setTextColor(menuActivity.getResources().getColor(R.color.transparent_grey));

            } catch (ParseException e) {
                e.printStackTrace();
            }




        }
        else
            mViewContract.setVisibility(View.GONE);

        ShowLocation();

        tvNameOfGym.setText(gym.getGym_name());
        tv_Location.setText(gym.getGym_address());
        tv_Adress.setText(gym.getGym_address());
        mAry = gym.getGym_timings();
        mViewBottom = MenuActivity.mViewBottom;

       MenuActivity.img_back_gymdetail.setVisibility(View.VISIBLE);
        MenuActivity.img_back_gymdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                menuActivity.getSupportFragmentManager().popBackStack();
            }
        });
        mViewBottom.setVisibility(View.GONE);



        mArrayTextView = new ArrayList<TextView>();
        mArrayTextView.add(mtvSun);
        mArrayTextView.add(mtvMon);
        mArrayTextView.add(mtvTeu);
        mArrayTextView.add(mtvWed);
        mArrayTextView.add(mtvThur);
        mArrayTextView.add(mtvFri);
        mArrayTextView.add(mtvSat);
        Calendar calendar = Calendar.getInstance();
        String dayName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());

        if (dayName.equalsIgnoreCase("Monday")) {
            SetTimings(dayName, mtvMon);
        } else if (dayName.equalsIgnoreCase("Tuesday")) {
            SetTimings(dayName, mtvTeu);
        } else if (dayName.equalsIgnoreCase("Wednesday")) {
            SetTimings(dayName, mtvWed);
        } else if (dayName.equalsIgnoreCase("Thursday")) {
            SetTimings(dayName, mtvThur);
        } else if (dayName.equalsIgnoreCase("Friday")) {
            SetTimings(dayName, mtvFri);
        } else if (dayName.equalsIgnoreCase("Saturday")) {
            SetTimings(dayName, mtvSat);
        } else if (dayName.equalsIgnoreCase("Sunday")) {
            SetTimings(dayName, mtvSun);
        }


        imgFilter.setVisibility(View.GONE);
        ArrayList<Services> mAarray = gym.getGym_features();
        gridLayoutManager = new GridLayoutManager(context, 2);
        rCyclerViewer.setHasFixedSize(true);
        rCyclerViewer.setLayoutManager(gridLayoutManager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(context, R.dimen.dp_3);
        rCyclerViewer.addItemDecoration(itemDecoration);
        gdAdopter = new GymDetailAdopter(mAarray, context);
        rCyclerViewer.setAdapter(gdAdopter);


    }



    public static void ExpiredDialog(Activity mActivity) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.trial_expired_dialog);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        TextView btnClose = (TextView) dialog.findViewById(R.id.bt_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        dialog.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_sunday:
                SetTimings("sunday", mtvSun);
                break;
            case R.id.tv_monday:
                SetTimings("monday", mtvMon);
                break;
            case R.id.tv_teusday:
                SetTimings("tuesday", mtvTeu);
                break;
            case R.id.tv_wednesday:
                SetTimings("wednesday", mtvWed);
                break;
            case R.id.tv_thursday:
                SetTimings("thursday", mtvThur);
                break;
            case R.id.tv_friday:
                SetTimings("friday", mtvFri);
                break;
            case R.id.tv_saturday:
                SetTimings("saturday", mtvSat);
                break;
            case R.id.view_adress:
                DirectionDialog();
                break;
            case R.id.img_backk:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.img_phone:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    CheckPermission();
                } else {
                    String contact_number = gym.getGym_phone();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + contact_number));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                break;
        }
    }

    private void CheckPermission() {
        if (ContextCompat.checkSelfPermission(menuActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED )
        {
            if (isNeverAskAgainCall) {
                ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Phone permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
            } else
               mFragment.requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 7);
        }
        else
        {
            String contact_number = gym.getGym_phone();
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" +contact_number));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    private void ShowPermissionWarning(final String retry, String desc) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_permission_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvHeading = (TextView) dialog.findViewById(R.id.tv_heading);
        TextView tvDesc = (TextView) dialog.findViewById(R.id.tv_desc);
        TextView tvRetry = (TextView) dialog.findViewById(R.id.tv_retry);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        tvHeading.setText("Phone Permission Required");
        tvDesc.setText(desc);
        tvRetry.setText(retry);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (retry.equalsIgnoreCase("retry")) {
                    CheckPermission();
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void SetTimings(String day, TextView tv) {

        for (int i = 0; i < gym.getGym_timings().size(); i++) {
            if (gym.getGym_timings().get(i).getDay().equalsIgnoreCase(day)) {
                String openingTime = gym.getGym_timings().get(i).getOpen_timing();
                String closingTime = gym.getGym_timings().get(i).getClose_timing();
                String openining_Time_Meridium = "";
                String closing_Time_Meridium = "";
                if (openingTime != null && !openingTime.equals("") || closingTime != null && !closingTime.equals("")) {
                    String[] splitStr = openingTime.split("\\s+");
                    String[] splitStrr = openingTime.split("\\s+");
                    openining_Time_Meridium = splitStr[1];
                    closing_Time_Meridium = splitStrr[1];
                }
                SelectedDay(tv);
                if (openingTime.equals("") || closingTime.equals("")) {

                    vClose.setVisibility(View.VISIBLE);
                    vOpen.setVisibility(View.GONE);

                }
                else
                {
                    vClose.setVisibility(View.GONE);
                    vOpen.setVisibility(View.VISIBLE);
                    SetTime(openingTime, closingTime, openining_Time_Meridium, closing_Time_Meridium);
                }
            }
        }
    }

    private void SelectedDay(TextView view) {

        for (int i = 0; i < mArrayTextView.size(); i++) {

            mArrayTextView.get(i).setBackground(context.getResources().getDrawable(R.drawable.day_uncheck));
            mArrayTextView.get(i).setTextColor(getResources().getColor(R.color.transparent_grey));

        }
        view.setBackground(context.getResources().getDrawable(R.drawable.day_check));
        view.setTextColor(getResources().getColor(R.color.white));

    }

    private void ShowLocation() {



        try {
            double latti = Double.parseDouble(gym.getLatitude());
            double longi = Double.parseDouble(gym.getLongitude());
            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_range_in);
            map.addMarker(new MarkerOptions().position(new LatLng(latti, longi)).visible(true).icon(icon).title(gym.getGym_address()));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latti, longi), 15));

            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    DirectionDialog();
                    return false;
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
        // }
    }







    public void DirectionDialog() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_direction_alert);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //    lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btnNo = (Button) dialog.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                String uri = "http://maps.google.com/maps?f=d&hl=en&saddr="+mGlobal.getLocation().split(",")[0]+","+mGlobal.getLocation().split(",")[1]+"&daddr="+gym.getLatitude()+","+gym.getLongitude();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                mContext.startActivity(Intent.createChooser(intent, "Select an application"));

            }
        });
        dialog.show();

    }










    public void TRAIN_NOW_API_RESULT(String gymCode) {
        mGlobal.SaveCode(gymCode);
        mGlobal.SaveGym(gym);
        mGlobal.saveShared(false);
        Utility.ReplaceFragment(new CodesFragment(), getFragmentManager());
    }

    private void SetTime(String openingTime, String closingTime, String opening_time_meridium, String closing_time_meridium) {
        String tim = openingTime;
        String[] hour = tim.split(":");
        String first = hour[0];
        String sec = hour[1];
        String[] second = sec.split("\\s+");
        String min = second[0];
        String meridium = second[1];

        String time = closingTime;
        String[] houre = time.split(":");
        String firste = houre[0];
        String sece = houre[1];
        String[] seconde = sece.split("\\s+");
        String mine = seconde[0];
        String meridiume = seconde[1];

        tvOpenTime.setText(opening_time_meridium);
        tvCloseTime.setText(closing_time_meridium);
        tvOpenningHr.setText(first);
        tvOpenningMin.setText(min);
        tvClosingMin.setText(firste);
        tvClosingHr.setText(mine);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 7:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    String contact_number = gym.getGym_phone();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+contact_number));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    if (!shouldShowRequestPermissionRationale(permissions[0])) {
                        isNeverAskAgainCall = true;
                        //ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    } else {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }

                break;

        }
    }
}
