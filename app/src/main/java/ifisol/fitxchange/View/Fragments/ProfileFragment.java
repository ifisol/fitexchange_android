package ifisol.fitxchange.View.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import ifisol.fitxchange.Controller.Async.ChangePasswordTask;
import ifisol.fitxchange.Controller.Async.InfoTask;
import ifisol.fitxchange.Controller.Async.UpDateProfileTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.LoginActivity;
import ifisol.fitxchange.View.Activities.MenuActivity;

/**
 * Created by MEH on 6/27/2016.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    ImageView imgcheckMale, imagCheckFmale;
    TextView tv_male, tv_female,tvDatePicker,tv_mmber_ID;
    ImageView imgFilter;
    Context context;
    ImageView imagProfile;
    Calendar calendar;
    View mViewMale,mViewFemale;
    public boolean isCheckedMale, isCheckedFemale;
    TextView haeder=MenuActivity.header;
    EditText edtFirstName,edtLastName,edtEmail,edPassword,edit_phone;
    TextView tv_user;
    GlobalActivity mGlobal;
    Context mContext;
    Button bt_editt_updat;
    Button bt_update_profle;
    String mGender="";
    ProfileFragment mProfil;
    MenuActivity mMenueActivity;
    View mViewEditProfile;
    View mViewUPdateProfile;
    View viewSignOut;
    View mViewProgressBar;
    Bitmap UserImage=null;
    EditText edConfirmPass;
    String newPass;
    TextView userEmail;
    TextView usernam;
    View viewParent;
    ProfileFragment fragmenttt;
    boolean isNeverAskAgainGallery=false;
    boolean isNeverAskAgainCamera=false;
    ProgressBar imgPb;

    View HomeView;
    TextView HomeTv;
    private static final int CAPTURE_MEDIA = 368;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View  view = inflater.inflate(R.layout.profile, container, false);
        return Init(view);

    }

    private View Init(View view) {
        this.context = getActivity();

        viewParent= view.findViewById(R.id.view_img_parent);
        fragmenttt=this;
        imgcheckMale = (ImageView) view.findViewById(R.id.checkbox_male);
        imagCheckFmale = (ImageView) view.findViewById(R.id.checkbox_female);
        mViewEditProfile= view.findViewById(R.id.view_edit_profile);
        mViewUPdateProfile= view.findViewById(R.id.view_update_profile);
        viewSignOut= view.findViewById(R.id.view_sign_out);
       // mViewProgressBar=(View)view.findViewById(R.id.view_progrss);
        mViewMale= view.findViewById(R.id.view_male);
        mViewFemale= view.findViewById(R.id.view_female);
        bt_editt_updat=(Button)view.findViewById(R.id.bt_edit_profile);
        bt_update_profle=(Button)view.findViewById(R.id.bt_update_profile);

        userEmail=(TextView)view.findViewById(R.id.tv_user_email);
        usernam=(TextView)view.findViewById(R.id.tv_usernm);
        HomeTv = (TextView) view.findViewById(R.id.tv_home_gym);
        HomeView = view.findViewById(R.id.view_home);
        edtFirstName=(EditText)view.findViewById(R.id.edt_first_name);
        edtLastName=(EditText)view.findViewById(R.id.edt_last_name);
        //edtEmail=(EditText)view.findViewById(R.id.edt_user_email);
        tv_user =(TextView)view.findViewById(R.id.tv_user_pass);
        imagProfile = (ImageView) view.findViewById(R.id.img_prof);
        tvDatePicker = (TextView) view.findViewById(R.id.tv_date_picker);
        edit_phone=(EditText)view.findViewById(R.id.ed_phon_numb);
        tv_female = (TextView) view.findViewById(R.id.tv_gender_female);
        tv_male = (TextView) view.findViewById(R.id.tv_gender_male);
        tv_mmber_ID=(TextView)view.findViewById(R.id.tv_member_id);
        imgPb = (ProgressBar) view.findViewById(R.id.progress_bar);

        imgFilter = MenuActivity.imgFilter;
        tvDatePicker.setOnClickListener(this);
        viewSignOut.setOnClickListener(this);

        HomeView.setOnClickListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AndroidBug5497Workaround.assistActivity(getActivity());
        mMenueActivity=(MenuActivity)getActivity();
        mMenueActivity.imgInfo.setVisibility(View.VISIBLE);
        MenuActivity.img_back_gymdetail.setVisibility(View.GONE);
        MenuActivity.imgHome.setVisibility(View.GONE);
        MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        MenuActivity.mViewBottom.setVisibility(View.VISIBLE);
        MenuActivity.imgSettings.setVisibility(View.VISIBLE);
        mProfil=this;
       // mContext=getActivity();
        viewParent.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        ViewEnable();
        mGender="male";
        haeder.setText("Profile");
        mGlobal=(GlobalActivity)context.getApplicationContext();
        SetData();
        calendar = Calendar.getInstance();
        imgFilter.setVisibility(View.GONE);
        isCheckedMale = true;
        isCheckedFemale = false;

        imagProfile.setOnClickListener(this);
        mViewMale.setOnClickListener(this);
        mViewFemale.setOnClickListener(this);
        bt_editt_updat.setOnClickListener(this);
        bt_update_profle.setOnClickListener(this);
        mMenueActivity.imgInfo.setOnClickListener(this);
        tv_user.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_prof:
               launchCamera();
               break;
            case R.id.view_home:
                Utility.ReplaceFragment(new OwnGymDetailFragment(),mMenueActivity.getSupportFragmentManager());
                break;
            case R.id.tv_date_picker:
                ShowDatePicker();
                break;
            case R.id.bt_edit_profile:
                if (!Utility.isConnected(context))
                {
                    mGlobal.ShoeDialog(getActivity());
                }
                else {
                    mViewEditProfile.setVisibility(View.GONE);
                    mViewUPdateProfile.setVisibility(View.VISIBLE);
                    ViewEnableTrue();
                }
                break;
            case R.id.bt_update_profile:
                UPDATE_PROFILE();
                break;
            case R.id.img_info:
                if (!Utility.isConnected(context))
                {
                    mGlobal.ShoeDialog(getActivity());
                }
                else {
                    new InfoTask(mMenueActivity,mProfil).execute();
                }
                break;
            case R.id.view_sign_out:
                GlobalActivity mGlobal=(GlobalActivity)context.getApplicationContext();
                mGlobal.SaveActiveUser(null);
                mGlobal.SaveCode(null);
                mGlobal.SaveGym(null);
                mGlobal.saveShared(false);
                Utility.OpenActivityPrevious(context,getActivity(), LoginActivity.class);
                break;
            case R.id.tv_user_pass:

                DialogChangePassword();
                break;



                    ///ShowDialog();
        }
    }
    public Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        //bitmap.recycle();

        return output;
    }

    private void DialogChangePassword() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.change_password_dialog);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final EditText edCurrentPass = (EditText) dialog.findViewById(R.id.edt_current_password);
        final EditText edNewPass = (EditText) dialog.findViewById(R.id.edt_new_password);
        edConfirmPass = (EditText) dialog.findViewById(R.id.edt_confirm_password);
        Button mBChagPass = (Button) dialog.findViewById(R.id.bt_chang_pass);
        Button mBCancel = (Button) dialog.findViewById(R.id.bt_camcel_pass);
        mBChagPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GlobalActivity mGlobal = (GlobalActivity) context.getApplicationContext();
                boolean isValid = true;
                if (!Utility.isConnected(context)) {
                    dialog.dismiss();
                    mGlobal.ShoeDialog(getActivity());
                } else {
                    if (edCurrentPass.getText().toString().isEmpty()) {
                        Utility.ShowAlertDialog(getActivity(), "Password", "Please enter current password");
                        isValid = false;
                    }
                    if (edNewPass.getText().toString().isEmpty()) {
                        if (isValid) {
                            Utility.ShowAlertDialog(getActivity(), "Password", "Please enter new password");
                            isValid = false;
                        }
                    }
                    if (edNewPass.getText().toString().length() < 6) {
                        if (isValid) {
                            Utility.ShowAlertDialog(getActivity(), "Password", "Password should contain at least 6 character");
                            isValid = false;
                        }
                    }
                    if (edConfirmPass.getText().toString().isEmpty()) {
                        if (isValid) {
                            Utility.ShowAlertDialog(getActivity(), "Password", "Please enter confirm password");
                            isValid = false;
                        }
                    }
                    if (!edNewPass.getText().toString().equals(edConfirmPass.getText().toString())) {
                        if (isValid) {
                            Utility.ShowAlertDialog(getActivity(), "Password", "Password doesn’t match");
                            isValid = false;
                        }
                    }
                    if (isValid) {
                        //APP CALLING
                        String oldPass = edCurrentPass.getText().toString();
                        newPass = edConfirmPass.getText().toString();
                        String User_id = mGlobal.GetActiveUser().getUser_id();
                        HashMap<String, String> param = new HashMap<>();
                        param.put("user_id", User_id);
                        param.put("password", oldPass);
                        param.put("new-password", newPass);
                        new ChangePasswordTask(mMenueActivity, mProfil, param).execute();
                        dialog.dismiss();
                    }

                }

            }
        });
        mBCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void ShowDatePicker()
    {
                final DatePickerDialog dpd = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
                dpd.setYearRange(1900, 2060);
                dpd.setMaxDate(calendar);
                dpd.setAccentColor(context.getResources().getColor(R.color.orange_ftx));
                dpd.setOnDateSetListener(this);
                FragmentManager fm = getActivity().getFragmentManager();
                dpd.show(fm, "");
    }
    @Override
    public void onDateSet(DatePickerDialog view,int year,int monthOfYear,int dayOfMonth) {
        tvDatePicker.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year));

    }






    public int getSquareCropDimensionForBitmap(Bitmap bitmap)
    {
        //use the smallest dimension of the image to crop to
        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }


    private void Click_EDIT_UODATE() {
        boolean isTrue=true;
        if (isTrue)
        {
            bt_editt_updat.setText("Update Profile");
            ViewEnableTrue();
            isTrue=false;
        }
        else
        {

        }
    }


    private void UPDATE_PROFILE() {

        String memberID = tv_mmber_ID.getText().toString();
        //String userNAME=usernam.getText().toString();
        String firstNAME = edtFirstName.getText().toString();
        String lastNAME = edtLastName.getText().toString();
        //String emaiL=edtEmail.getText().toString();
        String dateofBIRTH = tvDatePicker.getText().toString();
        String PHONE = edit_phone.getText().toString();
        boolean isValid = true;
        if (firstNAME.isEmpty()) {
            if (isValid)
                Utility.ShowAlertDialog(getActivity(), "First Name", "Please enter first name");
            isValid = false;

        }
        if (lastNAME.isEmpty()) {
            if (isValid)
                Utility.ShowAlertDialog(getActivity(), "Last Name", "Please enter last name");
            isValid = false;
        }
        if (dateofBIRTH.equals("")) {
            if (isValid)
                Utility.ShowAlertDialog(getActivity(), "Date of Birth", "Please select date of birth");
            isValid = false;
        }
        if (PHONE.isEmpty()) {
            if (isValid)
                Utility.ShowAlertDialog(getActivity(), "Phone No", "Please enter phone no");
            isValid = false;
        }
        if (isValid) {
            HashMap<String, String> param = new HashMap<>();
            if (UserImage != null)
                   param.put("photo", Utility.EncodeTobase64(UserImage));
                param.put("user_id", memberID);
                // param.put("user_name", userNAME);
                param.put("first_name", firstNAME);
                param.put("last_name", lastNAME);
                // param.put("email", emaiL);
                param.put("gender", mGender);
                param.put("phone", PHONE);
                param.put("dob", dateofBIRTH);
                new UpDateProfileTask(param, mMenueActivity, mProfil).execute();
                UserImage =null;

        }
    }


    private void ViewEnable() {
        imagProfile.setEnabled(false);
        edtFirstName.setEnabled(false);
        edtLastName.setEnabled(false);
        //edtEmail.setEnabled(false);
        tvDatePicker.setEnabled(false);
        tv_user.setEnabled(false);
        edit_phone.setEnabled(false);
        tv_mmber_ID.setEnabled(false);
        mViewMale.setEnabled(false);
        mViewFemale.setEnabled(false);
    }
    private void ViewEnableTrue() {
        imagProfile.setEnabled(true);
        edtFirstName.setEnabled(true);
        edtLastName.setEnabled(true);
        //edtEmail.setEnabled(true);
        tv_user.setEnabled(true);
        /*tvDatePicker.setEnabled(true);*/
        edit_phone.setEnabled(true);
        tv_mmber_ID.setEnabled(true);
       // mViewMale.setEnabled(true);
        //mViewFemale.setEnabled(true);
    }

    public void SetData()
    {
        usernam.setText(mGlobal.GetActiveUser().getUser_name());
        String userimg=mGlobal.GetActiveUser().getPhoto();
        if (userimg!=null && userimg.length()>0) {
            Picasso.with(context)
                    .load(Constants.Img_URL+userimg)
                    .error(R.drawable.ic_placholdr)
                    .into(imagProfile, new Callback() {
                        @Override
                        public void onSuccess() {
                            imgPb.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            imgPb.setVisibility(View.GONE);
                            imagProfile.setImageResource(R.drawable.ic_placholdr);
                        }
                    });
        }
        else
            imagProfile.setImageResource(R.drawable.ic_placholdr);
        edtFirstName.setText(mGlobal.GetActiveUser().getFirst_name());
        edtLastName.setText(mGlobal.GetActiveUser().getLast_name());
        userEmail.setText(mGlobal.GetActiveUser().getEmail());
        //tv_user.setText(mGlobal.GetActiveUser().getPassword());
        tvDatePicker.setText(mGlobal.GetActiveUser().getDob());
        edit_phone.setText(mGlobal.GetActiveUser().getPhone());
        tv_mmber_ID.setText(mGlobal.GetActiveUser().getUser_id());
        HomeTv.setText(mGlobal.GetActiveUser().getGym_name());
        String mGender=mGlobal.GetActiveUser().getGender();
        if (mGender.equalsIgnoreCase("male")) {
            imgcheckMale.setImageResource(R.drawable.male_check);
            tv_male.setTextColor(getResources().getColor(R.color.orange_ftx));
        }
        if (mGender.equalsIgnoreCase("female"))
        {
            imagCheckFmale.setImageResource(R.drawable.female_checked);
            tv_female.setTextColor(getResources().getColor(R.color.orange_ftx));
        }
    }

    public void UPDATE_API_RESULT()
    {
        mViewUPdateProfile.setVisibility(View.GONE);
        mViewEditProfile.setVisibility(View.VISIBLE);
        Toast.makeText(getActivity(), "Updated Profile Successfully", Toast.LENGTH_LONG).show();
       // SetData();
        Utility.ReplaceFragment(new ProfileFragment(), getFragmentManager());
        ViewEnable();
    }

    public void Profile_API_RESULT()
    {

    }


    public void API_RESULT_INFO(String url) {
        Bundle bundle=new Bundle();
       // bundle.putString("arg", agrement);
        bundle.putString("url", url);
        Utility.ReplaceFragment(new InfoFragment(), getFragmentManager(),bundle);

    }
    private void ShowPermissionWarning(final String retry, String desc,String s) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_permission_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvHeading = (TextView) dialog.findViewById(R.id.tv_heading);
        TextView tvDesc = (TextView) dialog.findViewById(R.id.tv_desc);
        TextView tvRetry = (TextView) dialog.findViewById(R.id.tv_retry);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        tvHeading.setText(s);
        tvDesc.setText(desc);
        tvRetry.setText(retry);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (retry.equalsIgnoreCase("retry")) {
                    CheckPermission();
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void CheckPermission() {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case 5:
                if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 1);
                }   else if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED )
                {
                    if (!shouldShowRequestPermissionRationale(permissions[0]) || !shouldShowRequestPermissionRationale(permissions[0]))
                    {
                        isNeverAskAgainGallery=true;
                        // ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    }
                    else
                    {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }

                break;
            case 4:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 0);
                       /* dialog.dismiss();*/
                }   else if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED )
                {
                    if (!shouldShowRequestPermissionRationale(permissions[0]) || !shouldShowRequestPermissionRationale(permissions[0]))
                    {
                        isNeverAskAgainCamera=true;
                        // ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    }
                    else
                    {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }
                break;
        }
    }

    private void launchCamera() {
        new SandriosCamera(getActivity(),this, CAPTURE_MEDIA)
                .setShowPicker(true)
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO) // default is CameraConfiguration.MEDIA_ACTION_BOTH
                .enableImageCropping(true) // Default is false.
                .launchCamera();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_MEDIA && resultCode == Activity.RESULT_OK) {
            try {
                UserImage = Utility.decodeScaledBitmap(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
                Log.e("File >>>>>>>>>>>>>", "" + data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
            } catch (IOException e) {
                e.printStackTrace();
            }


            if (UserImage!=null)
            {
                imagProfile.setImageResource(0);
                imagProfile.setImageBitmap(UserImage);
            }
        }
    }



}

