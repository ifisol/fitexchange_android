package ifisol.fitxchange.View.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import ifisol.fitxchange.Controller.Adapters.CodeCurrentAdopter;
import ifisol.fitxchange.Controller.Async.RegenerateCodeTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.Model.Services;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.FacebookSDKactivity;
import ifisol.fitxchange.View.Activities.MenuActivity;
import ifisol.fitxchange.View.Activities.TwitterActivity;

/**
 * Created by MEH on 6/24/2016.
 */
public class CodeCurrentFragment extends Fragment implements View.OnClickListener {
    RecyclerView rCyclerViewer;
    Context context;
    GridLayoutManager gridLayoutManager;
    CodeCurrentAdopter mAdopter;
    Gym gym;
    String gymCode;
    TextView tv_gym_name;
    TextView tv_adreess;
    TextView tv_websit;
    ImageView img_Phone;
    Context mContext;
    Handler mHandler;
    Runnable mRunnable;
    GlobalActivity mGlobal;
    TextView tvf;
    TextView tvS;
    TextView tvT;
    TextView tvFF;
    Dialog dialog;
    View viewDirect;
    View viewInDirect;
    private PendingIntent pendingIntent;
    AlarmManager manager;
    Timer timer;
    TimerTask timerTask;
    ImageView gymLogo;
    Button btnRegenerateCode;
    MenuActivity menuActivity;
    CodeCurrentFragment mFragment;
    Activity activity;
    boolean isNeverAskAgainCall=false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_codes_current, container, false);
        return Init(view);
    }

    private View Init(View view) {
        mContext=getActivity();
        activity=getActivity();
        mGlobal=(GlobalActivity)mContext.getApplicationContext();
        menuActivity=(MenuActivity)getActivity();
        mFragment=this;
        btnRegenerateCode=(Button)view.findViewById(R.id.bt_regenerate_code);
        rCyclerViewer=(RecyclerView)view.findViewById(R.id.recycler_code_current);
        tv_gym_name=(TextView)view.findViewById(R.id.tv_gymname);
        tv_adreess=(TextView)view.findViewById(R.id.tv_adresse);
        tv_websit=(TextView)view.findViewById(R.id.tv_website);
        img_Phone=(ImageView)view.findViewById(R.id.img_phone);
        tvf=(TextView)view.findViewById(R.id.tv_first);
        tvS=(TextView)view.findViewById(R.id.tv_second);
        tvT=(TextView)view.findViewById(R.id.tv_third);
        tvFF=(TextView)view.findViewById(R.id.tv_fourth);
        viewDirect= view.findViewById(R.id.view_direct);
        viewInDirect= view.findViewById(R.id.view_indirect);
        gymLogo=(ImageView)view.findViewById(R.id.img_gymlo);
        tv_websit.setOnClickListener(this);
        img_Phone.setOnClickListener(this);
        btnRegenerateCode.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mHandler=new Handler();

        gym = mGlobal.GetGym();


        if (gym == null) {
            viewDirect.setVisibility(View.VISIBLE);
            viewInDirect.setVisibility(View.GONE);
        } else {
            if (!mGlobal.GetIsShared())
                startTimer();
            gymCode = mGlobal.GetCode();
            viewInDirect.setVisibility(View.VISIBLE);
            viewDirect.setVisibility(View.GONE);
            String[] parts = gymCode.split("");
                tvf.setText(parts[1]);
                tvS.setText(parts[2]);
                tvT.setText(parts[3]);
                tvFF.setText(parts[4]);
            tv_gym_name.setText(gym.getGym_name());
            tv_adreess.setText(gym.getGym_address());
            String img=gym.getGym_logo();
            if (!img.equals("")&&img!=null) {
                Picasso.with(mContext)
                        .load(Constants.Img_URL + gym.getGym_logo())
                        .error(R.drawable.gym_avatar)
                        .into(gymLogo);
            }
            ArrayList<Services> mAarray = gym.getGym_features();
            gridLayoutManager = new GridLayoutManager(mContext, 2);
            rCyclerViewer.setHasFixedSize(true);
            rCyclerViewer.setLayoutManager(gridLayoutManager);
            ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mContext, R.dimen.dp_3);
            rCyclerViewer.addItemDecoration(itemDecoration);
            mAdopter = new CodeCurrentAdopter(mAarray, mContext);
            rCyclerViewer.setAdapter(mAdopter);
        }
    }

    private void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 5000);
    }

    private void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                mHandler.post(new Runnable() {
                    public void run() {
                            ShareDialog();
                    }
                });
            }

        };
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;

        }
    }

    public void ShareDialog() {
        mGlobal.saveShared(true);
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_share_dialog);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.CENTER);
        lp.copyFrom(window.getAttributes());

        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        Button btCancel=(Button)dialog.findViewById(R.id.bt_cancell);
        // ImageView canCel = (ImageView) dialog.findViewById(R.id.img_cancel);
        ImageView mfacbook=(ImageView)dialog.findViewById(R.id.facebook);
        ImageView mtwitter=(ImageView)dialog.findViewById(R.id.twitter);
        ImageView Instagram=(ImageView)dialog.findViewById(R.id.instagram);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mfacbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isPackageExisted(mContext,"com.facebook.katana"))
                {
                   // Utility.OpenActivityNext(mContext, activity, FacebookSDKactivity.class);
                    Intent i = new Intent(getActivity(), FacebookSDKactivity.class);
                    i.putExtra("Object", gym);
                    Utility.OpenActivityNextByData(i, getActivity());
                }
                else {
                    Toast.makeText(mContext, "Facebook Application is not installed", Toast.LENGTH_SHORT).show();

                }



           /*     String urll = "https://www.facebook.com/";
                try {
                    if (urll != null && !urll.equals("")) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(urll));
                        startActivity(i);
                    }
                } catch (Exception e) {
                    Toast.makeText(context, "Invalid URL", Toast.LENGTH_SHORT).show();
                }
            }*/
            }
        });
        mtwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*String urll = "https://twitter.com/login";
                try {
                    if (urll != null && !urll.equals("")) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(urll));
                        startActivity(i);
                    }
                } catch (Exception e) {
                    Toast.makeText(context, "Invalid URL", Toast.LENGTH_SHORT).show();
                }*/
                Intent i = new Intent(getActivity(), TwitterActivity.class);
                i.putExtra("Object", gym);
                Utility.OpenActivityNextByData(i,getActivity());


            }
        });
        Instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.instagram.com/FitExchangeGyms/"));
                startActivity(i);
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_website:
                String url = gym.getGym_website();
                try {
                    if (url!=null&&!url.equals("")) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(("http://" + url)));
                        startActivity(i);
                    }
                }
                catch (Exception e)
                {
                    Toast.makeText(context, "Invalid URL", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.img_phone:
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    CheckPermission();
                } else {
                    String contact_number = gym.getGym_phone();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" +contact_number));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                break;
            case  R.id.bt_regenerate_code:
                HashMap<String, String> param = new HashMap<>();
                String USER_ID=mGlobal.GetActiveUser().getUser_id();
                param.put("user_id",USER_ID);
                param.put("gym_id",gym.getGym_id());
                new RegenerateCodeTask(menuActivity,mFragment,param).execute();
                break;
        }

        }

    private void CheckPermission() {
        if (ContextCompat.checkSelfPermission(menuActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED )
        {
            if (isNeverAskAgainCall) {
                ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Phone permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
            } else
                mFragment.requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 8);
        }
        else
        {
            String contact_number = gym.getGym_phone();
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" +contact_number));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    private void ShowPermissionWarning(final String retry, String desc) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_permission_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvHeading = (TextView) dialog.findViewById(R.id.tv_heading);
        TextView tvDesc = (TextView) dialog.findViewById(R.id.tv_desc);
        TextView tvRetry = (TextView) dialog.findViewById(R.id.tv_retry);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        tvHeading.setText("Phone Permission Required");
        tvDesc.setText(desc);
        tvRetry.setText(retry);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (retry.equalsIgnoreCase("retry")) {
                    CheckPermission();
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onDestroyView(){
        super.onDestroy ();
        stoptimertask();
        super.onDestroyView();
    }

    public void TRAIN_NOW_API_RESULT(String gymCode) {
        mGlobal.SaveCode(gymCode);
        String[] parts = gymCode.split("");
        tvf.setText(parts[1]);
        tvS.setText(parts[2]);
        tvT.setText(parts[3]);
        tvFF.setText(parts[4]);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 8:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    String contact_number = gym.getGym_phone();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+contact_number));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    if (!shouldShowRequestPermissionRationale(permissions[0])) {
                        isNeverAskAgainCall = true;
                        //ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    } else {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }

                break;

        }
    }
}
