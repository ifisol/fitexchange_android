package ifisol.fitxchange.View.Fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ifisol.fitxchange.Model.Services;
import ifisol.fitxchange.R;

/**
 * Created by MEH on 6/29/2016.
 */
public class GymDetailAdopter extends  RecyclerView.Adapter<GymDetailAdopter.MyViewHolder> {
    ArrayList<Services> mArray=new ArrayList<Services>();

    Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_service_name;
        ImageView profileimage;


        public MyViewHolder(View view) {
            super(view);
            tv_service_name = (TextView) view.findViewById(R.id.tv_service);
            profileimage=(ImageView) view.findViewById(R.id.img_profile);
        }
    }
    public GymDetailAdopter( ArrayList<Services> mArray, Context context)
    {
        this.mArray=mArray;
        mContext=context;

    }

    @Override
    public GymDetailAdopter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_jym_detail_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GymDetailAdopter.MyViewHolder holder, int position) {
        Services obj=new Services();
        obj=mArray.get(position);
        holder.tv_service_name.setText(obj.getFeature_name());
        int resId=mContext.getResources().getIdentifier(obj.getFeature_img().split("\\.")[0],"drawable",mContext.getPackageName());
        holder.profileimage.setImageResource(resId);
    }

    @Override
    public int getItemCount() {
        return mArray.size();
    }
}
