package ifisol.fitxchange.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.BraintreePaymentActivity;
import com.braintreepayments.api.PaymentRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;

import java.util.ArrayList;

import ifisol.fitxchange.Controller.Adapters.PlansAdapter;
import ifisol.fitxchange.Controller.Async.GetPlansTask;
import ifisol.fitxchange.Controller.Async.SendNonceTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Subscription_Plans;
import ifisol.fitxchange.Model.Users;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.DemoActivity;
import ifisol.fitxchange.View.Activities.MenuActivity;

/**
 * Created by MEH on 8/2/2016.
 */
public class SettingsFragment extends Fragment{

    Context mContext;
    Activity mActivity;
    GlobalActivity mGlobal;
    SettingsFragment fragment;
    View mViewTrial;
    TextView tvExpDate;

    ListView mPlansList;
    PlansAdapter plansAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    public static final int REQUEST_CODE = 001;
    public Subscription_Plans plansObj;
    TextView tvFitXchange;
    ImageView imgTw, imgFb, imgInst;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        mPlansList = (ListView) view.findViewById(R.id.list_plans);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        tvExpDate = (TextView) view.findViewById(R.id.tv_date);
        mViewTrial = view.findViewById(R.id.trial_view);
        tvFitXchange = (TextView) view.findViewById(R.id.tv_fitx);
        imgFb   = (ImageView) view.findViewById(R.id.img_fb);
        imgTw   = (ImageView) view.findViewById(R.id.img_tw);
        imgInst = (ImageView) view.findViewById(R.id.img_inst);
        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mContext = getActivity();
        mActivity = getActivity();
        fragment  = this;
        mGlobal = (GlobalActivity) mActivity.getApplicationContext();

        MenuActivity.header.setText("Access Fit Exchange Gyms");



        MenuActivity.imgInfo.setVisibility(View.GONE);
        MenuActivity.imgHome.setVisibility(View.GONE);
        MenuActivity.mViewBottom.setVisibility(View.GONE);
        MenuActivity.imgSettings.setVisibility(View.GONE);
        MenuActivity.imgFilter.setVisibility(View.GONE);
        MenuActivity.img_back_gymdetail.setVisibility(View.VISIBLE);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange_ftx));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new GetPlansTask(fragment).execute(mGlobal.GetActiveUser().getUser_id());
            }
        });

        tvFitXchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String url = "http://fitexchange.com.au/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        imgFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String url = "https://www.facebook.com/FitExchangeGyms/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        imgTw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String url = "https://twitter.com/fitexchange";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        imgInst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String url = "https://www.instagram.com/FitExchangeGyms/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });

        MenuActivity.img_back_gymdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mGlobal.GetActiveUser().getIs_subscribed().equals("no") && mGlobal.GetActiveUser().getOn_trial().equals("no")) {
                   // Utility.ReplaceFragment(new DiscoverMapMainFragment(), getActivity().getSupportFragmentManager());
                    mGlobal.SaveActiveUser(null);
                    Utility.OpenActivityPrevious(mContext, getActivity(),DemoActivity.class);

                }
                // Toast.makeText(mContext, "You must have to subscribe a plan to proceed", Toast.LENGTH_SHORT).show();
                else
                {
                    if(getActivity().getSupportFragmentManager().getBackStackEntryCount() == 1)
                        Utility.ReplaceFragment(new DiscoverMainFragment(), getActivity().getSupportFragmentManager());
                    else
                        getActivity().getSupportFragmentManager().popBackStack();
                }

            }
        });

        MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        new GetPlansTask(this).execute(mGlobal.GetActiveUser().getUser_id());

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    PaymentMethodNonce paymentMethodNonce = data.getParcelableExtra(
                            BraintreePaymentActivity.EXTRA_PAYMENT_METHOD_NONCE
                    );
                    String nonce = paymentMethodNonce.getNonce();
                    new SendNonceTask(fragment).execute(nonce, mGlobal.GetActiveUser().getUser_id(),plansObj.getPlan_id(),"Active");

                    break;
                case BraintreePaymentActivity.BRAINTREE_RESULT_DEVELOPER_ERROR:
                    break;
                case BraintreePaymentActivity.BRAINTREE_RESULT_SERVER_ERROR:
                    break;
                case BraintreePaymentActivity.BRAINTREE_RESULT_SERVER_UNAVAILABLE:
                    break;
                // handle errors here, a throwable may be available in
                // data.getSerializableExtra(BraintreePaymentActivity.EXTRA_ERROR_MESSAGE)

                default:
                    break;
            }
        }
    }




    public void PlansApiRes(ArrayList<Subscription_Plans> mlist, String onTrial, String expiry)
    {
        MenuActivity.mViewProgressBar.setVisibility(View.GONE);



        if (onTrial.equals("yes"))
        {
            mViewTrial.setVisibility(View.VISIBLE);
            tvExpDate.setText(expiry);
            mPlansList.setVisibility(View.GONE);
        }
        else
        {
            mViewTrial.setVisibility(View.GONE);
            mPlansList.setVisibility(View.VISIBLE);
        }


        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        if (mlist!=null && mlist.size()>0)
        {
            if (plansAdapter==null)
            {
                plansAdapter = new PlansAdapter(mContext,mlist,this);
                mPlansList.setAdapter(plansAdapter);
            }
            else
            {
                plansAdapter.mArray=mlist;
                mPlansList.deferNotifyDataSetChanged();
            }

        }
        else Toast.makeText(mContext, "Error in loading Plans. Please try again later", Toast.LENGTH_SHORT).show();


    }


    public void InitializeBraintree(String token, Subscription_Plans obj)
    {
        //    mTvWaiting.setVisibility(View.GONE);
                plansObj = obj;
                PaymentRequest paymentRequest = new PaymentRequest()
                .clientToken(token)
                .primaryDescription("FitX Subscription")
                .secondaryDescription(obj.getPlan_name())
                .amount("$"+obj.getPlan_price())
                .submitButtonText("Subscribe")
                .disablePayPal();
                startActivityForResult(paymentRequest.getIntent(mContext), REQUEST_CODE);
    }
    public void PaymentAPI(String status) {
        try {
            int pos = plansAdapter.mArray.indexOf(plansObj);

            if (status.equalsIgnoreCase("Active")) {
                Toast.makeText(mActivity, "You have successfuly subscribed for " + plansObj.getPlan_name() + "", Toast.LENGTH_SHORT).show();
                plansAdapter.mArray.get(pos).setIs_subscribed("yes");
                Users user = (Users) mGlobal.GetActiveUser();
                user.setIs_subscribed("yes");
                plansAdapter.mArray.get(pos).setCan_un_subscribe("no");
                mGlobal.SaveActiveUser(user);

                if (getActivity().getSupportFragmentManager().getBackStackEntryCount() == 1)
                    Utility.ReplaceFragment(new DiscoverMainFragment(), getActivity().getSupportFragmentManager());
                else
                    getActivity().getSupportFragmentManager().popBackStack();

            } else {
                plansAdapter.mArray.get(pos).setIs_subscribed("no");
                Toast.makeText(mActivity, "Subscription has been cancelled successful", Toast.LENGTH_SHORT).show();
                Users user = (Users) mGlobal.GetActiveUser();
                user.setIs_subscribed("no");
                plansAdapter.mArray.get(pos).setCan_un_subscribe("yes");
                mGlobal.SaveActiveUser(user);
            }
            plansAdapter.notifyDataSetChanged();
            mPlansList.invalidate();
        }
        catch (Exception e)

        {
            e.printStackTrace();
        }
    }
}

