package ifisol.fitxchange.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.MenuActivity;

/**
 * Created by MEH on 8/2/2016.
 */
public class InfoFragment extends Fragment implements View.OnClickListener {

    WebView wV_browser;
    Context mContext;
    InfoFragment frag;
    MenuActivity mMenueActivity;
    TextView tvAgrement;
    TextView tvPrivacy;
    String agrement;
    String url;
    ImageView imgFacebook;
    ImageView imgTwitter;
    ImageView imgInstagram;
    Context context;
    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        return Init(view);

    }

    private View Init(View view) {
        imgFacebook = (ImageView) view.findViewById(R.id.img_facebook);
        imgTwitter=(ImageView)view.findViewById(R.id.img_twitter);
        imgInstagram=(ImageView)view.findViewById(R.id.img_instagram);
        wV_browser = (WebView) view.findViewById(R.id.wv_browser);
        wV_browser.getSettings().setJavaScriptEnabled(true);
        wV_browser.getSettings().setLoadWithOverviewMode(true);
        wV_browser.getSettings().setUseWideViewPort(true);
        tvAgrement = (TextView) view.findViewById(R.id.tv_agrement);
        tvPrivacy  = (TextView) view.findViewById(R.id.tv_privacy);
        imgFacebook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgInstagram.setOnClickListener(this);
        tvPrivacy.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        activity = getActivity();
        mMenueActivity = (MenuActivity) getActivity();
        mMenueActivity.imgInfo.setVisibility(View.GONE);
        MenuActivity.header.setText("Terms & Conditions");
        mContext = getActivity();
        //agrement = getArguments().getString("arg");
        url = "http://fitexchange.com.au/terms-conditions/";
        frag = this;
        MenuActivity.mViewBottom.setVisibility(View.GONE);
        MenuActivity.imgSettings.setVisibility(View.GONE);
        MenuActivity.img_back_gymdetail.setVisibility(View.VISIBLE);
        //MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        MenuActivity.img_back_gymdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getFragmentManager().findFragmentById(R.id.fram);
                if (fragment instanceof InfoFragment) {
                    getFragmentManager().popBackStack();
                }
            }
        });
        wV_browser.setWebViewClient(new MyWebViewClient());
        wV_browser.getSettings().setJavaScriptEnabled(true);
        if (!url.equals("")||url!=null) {
            wV_browser.loadUrl(url);
        } else {
            MenuActivity.mViewProgressBar.setVisibility(View.GONE);
        }
        // new InfoTask(mMenueActivity,frag).execute();

    }
    @Override
    public void onDestroy() {
        wV_browser.destroy();
        wV_browser = null;
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        String urll = "https://www.facebook.com/FitExchangeGyms/";
        String tweetUrl="https://twitter.com/fitexchange";
        String intagramUrl="https://www.instagram.com/FitExchangeGyms/";

        switch (v.getId()) {
            case R.id.img_facebook:
            try {
                if (urll != null && !urll.equals("")) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(urll));
                    startActivity(i);
                }
            } catch (Exception e) {
                Toast.makeText(context, "Invalid URL", Toast.LENGTH_SHORT).show();
            }
                break;
            case R.id.img_twitter:
                if (tweetUrl != null && !tweetUrl.equals("")) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(tweetUrl));
                    startActivity(i);
                }
                break;
            case R.id.img_instagram:
                if (intagramUrl != null && !intagramUrl.equals("")) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(intagramUrl));
                    startActivity(i);
                }
                break;

            case R.id.tv_privacy:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://fitexchange.com.au/privacy-policy/"));
                startActivity(i);
                break;
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (view!=null) {
                MenuActivity.mViewProgressBar.setVisibility(View.GONE);
            }
            super.onPageFinished(view, url);
        }


    }
}

