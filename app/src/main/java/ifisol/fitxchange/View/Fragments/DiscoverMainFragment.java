package ifisol.fitxchange.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import ifisol.fitxchange.Controller.Async.InfoTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.MenuActivity;

/**
 * Created by MEH on 8/30/2016.
 */
public class DiscoverMainFragment extends Fragment implements View.OnClickListener {
    View viewTOlisting;
    View viewTomap;
    static View viewlist;
    static View viewMap;
    Context mContext;
    FrameLayout layout;
    GlobalActivity mGlobal;
    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view==null)
        {
            view=inflater.inflate(R.layout.fragment_discovermapmain, container, false);
        }
        viewTOlisting= view.findViewById(R.id.view_to_listing);
        viewTomap= view.findViewById(R.id.view_to_map);
        viewlist= view.findViewById(R.id.view_listing);
        viewMap= view.findViewById(R.id.view_map);
        layout=(FrameLayout)view.findViewById(R.id.fram_layout);
        return  view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext=getActivity();
        mGlobal = (GlobalActivity) mContext.getApplicationContext();
        MenuActivity.mViewBottom.setVisibility(View.VISIBLE);
        MenuActivity.header.setText("Explore");
        MenuActivity.img_back_gymdetail.setVisibility(View.GONE);
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fram_layout);
        if (fragment instanceof DiscoverFragment)
        {
            viewMap.setVisibility(View.GONE);
            viewlist.setVisibility(View.VISIBLE);
            MenuActivity.imgFilter.setVisibility(View.VISIBLE);
        }
        else if (fragment instanceof MapFragment)
        {
            viewMap.setVisibility(View.VISIBLE);
            viewlist.setVisibility(View.GONE);
        }
        viewTOlisting.setOnClickListener(this);
        viewTomap.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fram_layout);
        switch (v.getId())
        {
            case R.id.view_to_listing:
                if (fragment instanceof DiscoverFragment)
                {

                }
                else
                {
                    viewMap.setVisibility(View.GONE);
                    viewlist.setVisibility(View.VISIBLE);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.fram_layout, new DiscoverFragment());
                    transaction.addToBackStack(null);
                    transaction.commit();

                }
                break;
            case R.id.view_to_map:
                if (!Utility.isConnected(mContext))
                {
                    mGlobal.ShoeDialog(getActivity());
                }
                else {
                    if (fragment instanceof DiscoverFragment)
                    {
                        if ((((DiscoverFragment) fragment).Array==null)||(((DiscoverFragment) fragment).Array.size()==0))
                        {
                           // Toast.makeText(mContext, "No Gym found", Toast.LENGTH_LONG).show();

                        }
                        else {
                            viewMap.setVisibility(View.VISIBLE);
                            viewlist.setVisibility(View.GONE);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("array", ((DiscoverFragment) fragment).Array);
                            bundle.putBoolean("IS_HOME", ((DiscoverFragment) fragment).isNearHomeGym);
                            MapFragment mapFragment = new MapFragment();
                            mapFragment.setArguments(bundle);
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.addToBackStack("");
                            transaction.replace(R.id.fram_layout, mapFragment);
                            transaction.commit();
                        }
                    }
                    else if (fragment instanceof MapFragment)
                    {

                    }

                }

                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fram_layout, new DiscoverFragment());
        transaction.addToBackStack("");
        transaction.commit();
    }
}
