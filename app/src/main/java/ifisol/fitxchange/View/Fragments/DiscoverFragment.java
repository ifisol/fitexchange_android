package ifisol.fitxchange.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ifisol.fitxchange.Controller.Adapters.DiscoverAdopter;
import ifisol.fitxchange.Controller.Async.SearchGymTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Activities.MenuActivity;

/**
 * Created by MEH on 6/22/2016.
 */
public class DiscoverFragment extends Fragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    ListView listVdiscover;
    public  DiscoverAdopter discoverAdopter;
    View mViewBottom;
    TextView haeder=MenuActivity.header;
    ImageView imgFilter;
    ImageView img_back;
    DiscoverFragment discoverFragment;
    MenuActivity mMenueActivity;
    GlobalActivity mGlobal;
    Context context;
    View viewParent;
    ArrayList<Gym> Array;
    View nogym;
    private SwipeRefreshLayout swipeRefreshLayout;
    public boolean isNearHomeGym;
    View viewExplore;
    View viewReferesh;
    Button btReferesh;
    TextView tvNoRecord;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discover, container, false);
        return Init(view);
    }

    private View Init(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange_ftx));
        swipeRefreshLayout.setRefreshing(true);
        viewParent= view.findViewById(R.id.view_parent);

        viewExplore=(View)view.findViewById(R.id.view_explore);
        viewReferesh=(View)view.findViewById(R.id.view_referesh);
        btReferesh=(Button)view.findViewById(R.id.bt_refresh);
        tvNoRecord = (TextView) view.findViewById(R.id.tv_no_record);

        listVdiscover=(ListView)view.findViewById(R.id.list_discover);
        imgFilter= MenuActivity.imgFilter;
        context=getActivity();
        mGlobal=(GlobalActivity)context.getApplicationContext();
        return view;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMenueActivity=(MenuActivity)getActivity();
        mMenueActivity.imgInfo.setVisibility(View.GONE);
        MenuActivity.imgFilter.setVisibility(View.GONE);
        viewParent.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        discoverFragment=this;
        DiscoverMainFragment.viewMap.setVisibility(View.GONE);
        DiscoverMainFragment.viewlist.setVisibility(View.VISIBLE);
        this.context=getActivity();
      /*  if (mGlobal.mLocation.getLocc()!=null && mGlobal.mLocation.getLocc().length()>0)
        {
            SearchGym();
        }*/
        SearchGym();

      //  DiscoverMapMainFragment.viewlist.setVisibility(View.VISIBLE);
       // DiscoverMapMainFragment.viewMap.setVisibility(View.GONE);

        haeder.setText("Explore");
        mViewBottom=MenuActivity.mViewBottom;
        mViewBottom.setVisibility(View.VISIBLE);
        img_back=MenuActivity.img_back_gymdetail;
        img_back.setVisibility(View.GONE);


       /* mGlobal.mLocation.getLocc();*/
        listVdiscover.setOnItemClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        btReferesh.setOnClickListener(this);
    }

    private void SearchGym() {
        if (!Utility.isConnected(context))
        {
            mGlobal.ShoeDialog(mMenueActivity);
            viewReferesh.setVisibility(View.VISIBLE);
            viewExplore.setVisibility(View.GONE);
        }
        else if (mGlobal.getLocation() != null && mGlobal.getLocation().length() > 0) {
            new SearchGymTask(mMenueActivity, discoverFragment, "progressbar_yes",context).execute();
        }
        else
        {
            LocationManager locationManager = (LocationManager)context.getSystemService(context.LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            }else{
                showGPSDisabledAlertToUser();
            }
            viewReferesh.setVisibility(View.VISIBLE);
            viewExplore.setVisibility(View.GONE);
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MenuActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        Gym gym=(Gym)discoverAdopter.getItem(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("obj",gym);
        bundle.putString("str","discover");
        bundle.putBoolean("IS_HOME",isNearHomeGym);
        Utility.ReplaceFragment(new GymDetailFragment(),getFragmentManager(),bundle);
    }
    public void API_Result(ArrayList<Gym> mArray, boolean isHome)
    {
        isNearHomeGym = isHome;
        Array=mArray;
        if (isNearHomeGym)
            MenuActivity.imgHome.setVisibility(View.VISIBLE);
        else
            MenuActivity.imgHome.setVisibility(View.GONE);

        if (Array==null||Array.size()==0)
        {
            viewReferesh.setVisibility(View.VISIBLE);
            viewExplore.setVisibility(View.GONE);
            MenuActivity.imgFilter.setVisibility(View.GONE);
        }
        else {
            viewReferesh.setVisibility(View.GONE);
            viewExplore.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setRefreshing(false);
            MenuActivity.imgFilter.setVisibility(View.VISIBLE);
            ArrayList<Gym> mGymsIn30km = new ArrayList<>();
            for (Gym obj : Array)
            {
                if (obj.getWithin_range().equals("true"))
                    mGymsIn30km.add(obj);
            }

            if (mGymsIn30km==null || mGymsIn30km.size()==0)
            {
                viewReferesh.setVisibility(View.VISIBLE);
                tvNoRecord.setText("No Gyms found in your 30 km radius\nPlease check Map for all Gyms");
                viewExplore.setVisibility(View.GONE);
                MenuActivity.imgFilter.setVisibility(View.GONE);
            }
            else
            {
                viewReferesh.setVisibility(View.GONE);
                viewExplore.setVisibility(View.VISIBLE);
                MenuActivity.imgFilter.setVisibility(View.VISIBLE);
                discoverAdopter = new DiscoverAdopter(context, mGymsIn30km);
            /*discoverAdopter.mArray.clear();*/
                listVdiscover.setAdapter(discoverAdopter);
                Collections.sort(discoverAdopter.mArray, DistanceComparator);
            }



        }

        MenuActivity.mViewProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {

        if (!Utility.isConnected(context))
        {
            mGlobal.ShoeDialog(mMenueActivity);
            viewReferesh.setVisibility(View.VISIBLE);
            viewExplore.setVisibility(View.GONE);
        }
        else if (mGlobal.getLocation() != null && mGlobal.getLocation().length() > 0) {
            new SearchGymTask(mMenueActivity, discoverFragment, "progressbar_yes",context).execute();
            MenuActivity.mViewProgressBar.setVisibility(View.GONE);

        }
        else
        {
            LocationManager locationManager = (LocationManager)context.getSystemService(context.LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            }else{
                showGPSDisabledAlertToUser();
            }
            viewReferesh.setVisibility(View.VISIBLE);
            viewExplore.setVisibility(View.GONE);
        }
       /* if (mGlobal.mLocation.getLocc() != null && mGlobal.mLocation.getLocc().length() > 0) {
            new SearchGymTask(mMenueActivity, discoverFragment, "progressbar_no").execute();
        }*/
    }

   /* @Override
    public void onRefresh() {
        discoverAdopter.mArray.clear();

    }*/
    public static Comparator<Gym> DistanceComparator = new Comparator<Gym>() {

        public int compare(Gym gym1, Gym gym2) {
            double mvalue = Double.parseDouble( String.valueOf(gym1.getDistance_in_km()) );
            double mvalue2 = Double.parseDouble( String.valueOf(gym2.getDistance_in_km()) );
            return Double.compare(mvalue,mvalue2);
        }

    };

    public void No_Gym_Found() {
        viewReferesh.setVisibility(View.VISIBLE);
        viewExplore.setVisibility(View.GONE);
        MenuActivity.imgFilter.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (!Utility.isConnected(context))
        {
            mGlobal.ShoeDialog(mMenueActivity);
            viewReferesh.setVisibility(View.VISIBLE);
            viewExplore.setVisibility(View.GONE);
        }
        else if (mGlobal.getLocation() != null && mGlobal.getLocation().length() > 0) {
            new SearchGymTask(mMenueActivity, discoverFragment, "progressbar_yes",context).execute();
        }
        else
        {
            LocationManager locationManager = (LocationManager)context.getSystemService(context.LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            }else{
                showGPSDisabledAlertToUser();
            }
            viewReferesh.setVisibility(View.VISIBLE);
            viewExplore.setVisibility(View.GONE);
        }
    }

    private void showGPSDisabledAlertToUser() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_gps);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        Button mBttonCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        mBttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* HashMap<String, String> param = new HashMap<>();
                {
                    String user_id=mGlabal_obj.GetActiveUser().getUser_id();
                    param.put("user_id", user_id);
                    new UnRegisterUserTask(mActivity,param).execute();
                    dialog.dismiss();
                }*/
                dialog.dismiss();
            }
        });
        dialog.show();

    }
}
