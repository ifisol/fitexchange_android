package ifisol.fitxchange.View.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import ifisol.fitxchange.Controller.Async.CodeVerifyTask;
import ifisol.fitxchange.Controller.Async.REsendCodeTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Users;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;

/*import com.google.android.gms.games.Game;*/

/**
 * Created by MEH on 7/20/2016.
 */
public class VerificationCodeActivity extends Activity implements View.OnClickListener {
    TextView mtvCancel,mtvVerify;
    Context mContext;
    VerificationCodeActivity mActivity;
    String code;
    String userCode;
    public View mViewProgressBar;
    String firstDigit;
    EditText ed_first,ed_second,ed_third,ed_fourth;
    GlobalActivity mGlabal_obj;
    TextView tvResendCode;
    Users users;
    String one;
    String two;
    String three;
    String four;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);
        mContext=this;
        mGlabal_obj=(GlobalActivity)mContext.getApplicationContext();
        mViewProgressBar= findViewById(R.id.progress_bar);
        tvResendCode=(TextView)findViewById(R.id.tv_resend_code);
        Int();
        Intent intent = getIntent();
        users = (Users) intent.getExtras().getSerializable("obj");
       // Users user = (Users)mGlabal_obj.GetActiveUser();
        users.setIs_subscribed("no");
       // mGlabal_obj.SaveActiveUser(user);
        mActivity=this;
        Code_Confirmation_Dialog("A notification code was sent to your email and Phone number.Please check your Email or Phone number.");
        ed_first.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    if (ed_second.getText().toString().isEmpty()) {
                        ed_second.requestFocus();
                    } else if (ed_third.getText().toString().isEmpty()) {
                        ed_third.requestFocus();
                    } else if (ed_fourth.getText().toString().isEmpty()) {
                        ed_fourth.requestFocus();
                    }

                }

            }
        });
        ed_second.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    if (ed_first.getText().toString().isEmpty()) {
                        ed_first.requestFocus();
                    } else if (ed_third.getText().toString().isEmpty()) {
                        ed_third.requestFocus();
                    } else if (ed_fourth.getText().toString().isEmpty()) {
                        ed_fourth.requestFocus();
                    }
                }
            }
        });
        ed_third.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    if (ed_first.getText().toString().isEmpty()) {
                        ed_first.requestFocus();
                    } else if (ed_second.getText().toString().isEmpty()) {
                        ed_second.requestFocus();
                    } else if (ed_fourth.getText().toString().isEmpty()) {
                        ed_fourth.requestFocus();
                    }
                }
            }
        });
        ed_fourth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    if (ed_first.getText().toString().isEmpty()) {
                        ed_first.requestFocus();
                    } else if (ed_second.getText().toString().isEmpty()) {
                        ed_second.requestFocus();
                    } else if (ed_third.getText().toString().isEmpty()) {
                        ed_third.requestFocus();
                    }
                }
            }
        });
    }


    private void Int() {
        mContext=this;
        mActivity=this;
        mtvCancel=(TextView)findViewById(R.id.tv_cancel);
        mtvVerify=(TextView)findViewById(R.id.tv_verify);
        ed_first=(EditText)findViewById(R.id.edt_first);
        ed_second=(EditText)findViewById(R.id.edt_second);
        ed_third=(EditText)findViewById(R.id.edt_third);
        ed_fourth=(EditText)findViewById(R.id.edt_fourth);
        mViewProgressBar= findViewById(R.id.view_progrss);
        mtvCancel.setOnClickListener(this);
        mtvVerify.setOnClickListener(this);
        tvResendCode.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_cancel:
                ShowConfirmCancelDialog();
                break;
            case R.id.tv_verify:
                VerifyCode();
                break;
            case R.id.tv_resend_code:
                RESEND_CODE();
                break;
        }

    }

    private void RESEND_CODE() {
        if (!Utility.isConnected(mContext)) {
            mGlabal_obj.ShoeDialog(mActivity);
        } else {
            ed_first.requestFocus();
            ed_first.setText("");
            ed_second.setText("");
            ed_third.setText("");
            ed_fourth.setText("");
            HashMap<String, String> param = new HashMap<>();
            String user_id = users.getUser_id();
            param.put("user_id", user_id);
            new REsendCodeTask(mActivity, param).execute();

        }
    }

    private void ShowConfirmCancelDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_unregister_user);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        final EditText edt_mail=(EditText)dialog.findViewById(R.id.edt_eemail);
        Button mBttonok = (Button) dialog.findViewById(R.id.bt_ok);
        Button mBttonCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        mBttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* HashMap<String, String> param = new HashMap<>();
                {
                    String user_id=mGlabal_obj.GetActiveUser().getUser_id();
                    param.put("user_id", user_id);
                    new UnRegisterUserTask(mActivity,param).execute();
                    dialog.dismiss();
                }*/
                finish();
            }
        });
        mBttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                      dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void VerifyCode() {
        boolean isValid=true;
        if (!Utility.isConnected(mContext))
        {
            mGlabal_obj.ShoeDialog(mActivity);
            isValid=false;
        }
      /*  if (ed_first.getText().toString().length()>1)
        {
            if (isValid)
                Utility.ShowAlertDialog(mActivity, "Verify Code", "You can'nt enter more then one value in one  box");
                isValid = false;
         }
        if (ed_second.getText().toString().length()>1)
        {
            if (isValid)
                Utility.ShowAlertDialog(mActivity,"Verify Code", "You can'nt enter more then one value in one  box");
            isValid = false;
        }
        if (ed_third.getText().toString().length()>1)
        {
            if (isValid)
                Utility.ShowAlertDialog(mActivity, "Verify Code", "You can'nt enter more then one value in one  box");
            isValid = false;
        }
        if (ed_fourth.getText().toString().length()>1)
        {
            if (isValid)
                Utility.ShowAlertDialog(mActivity,"Verify Code", "You can'nt enter more then one value in one box");
                isValid = false;
        }*/
        if (ed_fourth.getText().toString().isEmpty()&&ed_second.getText().toString().isEmpty()&&ed_third.getText().toString().isEmpty()&&ed_fourth.getText().toString().isEmpty())
        {
            if (isValid)
                Utility.ShowAlertDialog(mActivity,"Verify Code", "Please complete all the fields");
            isValid = false;
        }
        if (!Utility.isConnected(mContext))
        {
            if (isValid)
            {
                mGlabal_obj.ShoeDialog(mActivity);
                isValid = false;
            }
        }

        if (isValid) {
            one = ed_first.getText().toString();
            two = ed_second.getText().toString();
            three = ed_third.getText().toString();
            four = ed_fourth.getText().toString();
            String codeString = one;
            codeString = codeString.concat(two);
            String codeSubString = codeString;
            codeSubString = codeSubString.concat(three);
            String code = codeSubString;
            code = code.concat(four);

            String userID = users.getUser_id();
            HashMap<String, String> param = new HashMap<>();
            param.put("reg_code", code);
            param.put("user_id", userID);
            new CodeVerifyTask(mActivity, param).execute();
        }
    }
    public void API_RESULT(){
        mGlabal_obj.SaveActiveUser(users);
        mActivity.mViewProgressBar.setVisibility(View.GONE);

        if (users.getOn_trial().equals("yes"))
            Utility.OpenActivityNext(mContext,VerificationCodeActivity.this, Trial_congrats_activity.class);
        else
            Utility.OpenActivityNext(mContext,VerificationCodeActivity.this, MenuActivity.class);
    }
   @Override
    public void onBackPressed() {
       mGlabal_obj.SaveActiveUser(null);
    }
    public void API_RESULT_DELETE_USER()
    {
        mGlabal_obj.SaveActiveUser(null);
        finish();
        mActivity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }
    public void API_RESPONSE() {
        Code_Confirmation_Dialog("A new verification code has been sent to your Email and Phone number.Please check your Email or Phone number.");
    }
    private void Code_Confirmation_Dialog(String ms) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_code_confirmation);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        TextView tvMessage=(TextView)dialog.findViewById(R.id.tv_message);
        tvMessage.setText(ms);
        final EditText edt_mail=(EditText)dialog.findViewById(R.id.edt_eemail);
        Button mBttonok = (Button) dialog.findViewById(R.id.bt_ok);
        mBttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
