package ifisol.fitxchange.View.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.soundcloud.android.crop.Crop;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import ifisol.fitxchange.Controller.Adapters.GymListingAdapter;
import ifisol.fitxchange.Controller.Async.GetGymListTask;
import ifisol.fitxchange.Controller.Async.SignUp;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.Model.Users;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;

/**
 * Created by MEH on 6/21/2016.
 */
public class RegisterActivity extends Activity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    View vLogin;
    Calendar calendar;
    String mGender;
    ArrayList<Gym> mGymList;
    ProgressBar mProg;
    TextView mTvGymName;
    TextView tv_male, tv_female,tv_DateOvBirth;
    ImageView iVBack,imgcheckMale, imagCheckFmale,ImageViewProfile;
    ImageView imageMassage;
    View mVmAle,mVfEmale,mViewSelectImage;
    public boolean isCheckedMale, isCheckedFemale;
    Context mContext;
    String Gym_ID;
    RegisterActivity mActivity;
    Button btRegister;
    Bitmap UserImage=null;
    public View mViewProgressBar;
    GlobalActivity  obj_global;
    boolean isChecked;
    EditText edt_UserName,edt_fname,edt_lname,edt_email,edt_password,edt_gym_name,edt_phon_number,edt_birth_date;
    EditText ConfrmPASS;
    EditText EdReff;
    CheckBox checkBox;
    TextView tv_TermsCond;
    Dialog dialog;
    boolean isNeverAskAgainGallery=false;
    boolean isNeverAskAgainCamera=false;
    String img="";
    private static final int CAPTURE_MEDIA = 368;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signuptest);
        mActivity=this;
   //     AndroidBug5497Workaround.assistActivity(mActivity);
        calendar = Calendar.getInstance();
        isCheckedMale = true;
        isCheckedFemale = false;
        isChecked=false;
        mContext=this;
        mGender="Male";



        tv_TermsCond=(TextView)findViewById(R.id.tv_trms);

       // mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mProg=(ProgressBar)findViewById(R.id.progress_bar);
        obj_global = (GlobalActivity)mContext.getApplicationContext();
        vLogin = findViewById(R.id.v_tv_login);
        iVBack = (ImageView) findViewById(R.id.img_back);
        btRegister=(Button)findViewById(R.id.bt_register);
        imgcheckMale = (ImageView)findViewById(R.id.checkbox_male);
        imagCheckFmale = (ImageView)findViewById(R.id.checkbox_female);
        tv_male = (TextView)findViewById(R.id.tv_gender_male);
        ConfrmPASS=(EditText)findViewById(R.id.edt_confpass);
        EdReff=(EditText)findViewById(R.id.edt_ref_code);
        tv_DateOvBirth=(TextView)findViewById(R.id.tv_birth_date);
        tv_female = (TextView)findViewById(R.id.tv_gender_female);
        mTvGymName= (TextView)findViewById(R.id.tv_gym_name);
       // tv_male.setTextColor(getResources().getColor(R.color.white));
       // tv_female.setTextColor(getResources().getColor(R.color.white));
        mVmAle= findViewById(R.id.v_male);
        mVfEmale= findViewById(R.id.v_female);

        checkBox=(CheckBox)findViewById(R.id.chk_box);
        mViewProgressBar= findViewById(R.id.view_progrss);
        ImageViewProfile=(ImageView)findViewById(R.id.img_profil);
        imageMassage=(ImageView)findViewById(R.id.profile_image);
        edt_UserName=(EditText)findViewById(R.id.edt_Uname);
        edt_fname=(EditText)findViewById(R.id.edt_fnamee);
        edt_lname=(EditText)findViewById(R.id.edt_lname);
        edt_email=(EditText)findViewById(R.id.edt_email);
        edt_password=(EditText)findViewById(R.id.edt_password);
       // edt_gym_name=(EditText)findViewById(R.id.edt);
        edt_phon_number=(EditText)findViewById(R.id.edt_phone_numb);


        mViewSelectImage= findViewById(R.id.view_select_image);
        mVmAle.setOnClickListener(this);
        mVfEmale.setOnClickListener(this);
        vLogin.setOnClickListener(this);
        btRegister.setOnClickListener(this);
        iVBack.setOnClickListener(this);
        tv_DateOvBirth.setOnClickListener(this);
        mViewSelectImage.setOnClickListener(this);
        mTvGymName.setOnClickListener(this);
        tv_TermsCond.setOnClickListener(this);
        edt_phon_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String x = s.toString();
                if(x.startsWith("0"))
                {
                    edt_phon_number.setText("");
                    Toast.makeText(mContext, "The first digit can not be 0", Toast.LENGTH_SHORT).show();
                }

            }
        });
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isChecked) {
                    isChecked=true;
                }
                else
                {
                    isChecked=false;
                }
            }
        });

        new GetGymListTask(mActivity).execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                mActivity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
                break;
            case R.id.v_tv_login:
                Utility.OpenActivityPrevious(mContext, RegisterActivity.this,LoginActivity.class);
                break;
            case R.id.bt_register:
                //Utility.OpenActivityNext(mContext,RegisterActivity.this,VerificationCodeActivity.class);
                RegisterUser();
                break;
            case R.id.v_male:
                imgcheckMale.setImageResource(R.drawable.male_check);
                imagCheckFmale.setImageResource(R.drawable.fem_check_2);
               tv_male.setTextColor(getResources().getColor(R.color.orange_ftx));
                tv_female.setTextColor(getResources().getColor(R.color.white));
                mGender="Male";
                isCheckedMale = true;
                isCheckedFemale = false;
                break;
            case R.id.v_female:
                imagCheckFmale.setImageResource(R.drawable.female_checked);
                imgcheckMale.setImageResource(R.drawable.male_check2);
                mGender="Female";
                tv_male.setTextColor(getResources().getColor(R.color.white));
                tv_female.setTextColor(getResources().getColor(R.color.orange_ftx));
                isCheckedMale = false;
                isCheckedFemale = true;
                break;
            case R.id.view_select_image:
                launchCamera();
                break;
            case R.id.tv_birth_date:
                ShowDatePicker();
                break;
            case R.id.tv_gym_name:
                if (mGymList==null || mGymList.size()==0)
                    Toast.makeText(mContext, "No Gyms available. Please try again later", Toast.LENGTH_SHORT).show();
                else
                ShowGymsDialog();
                break;
            case R.id.tv_trms:
                String urll = "http://fitexchange.com.au/terms-conditions/";
                    if (urll != null && !urll.equals("")) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(urll));
                        startActivity(i);
                    }
        }
    }

   private void RegisterUser() {
       boolean isValid = true;
       String mUserName = edt_UserName.getText().toString();
       String fName = edt_fname.getText().toString();
       String lName = edt_lname.getText().toString();
       String eMail = edt_email.getText().toString().trim();
       String mPassword = edt_password.getText().toString();
       String mPhoneNum = "+61"+edt_phon_number.getText().toString();
       String dateofBirth = tv_DateOvBirth.getText().toString();
       if (!Utility.isConnected(mContext))
       {
               obj_global.ShoeDialog(mActivity);
               isValid=false;
       }
       if (UserImage==null) {
           if (isValid) {
               Utility.ShowAlertDialog(mActivity, "Profile Image", "Please select profile image it's compulsory");
               isValid = false;
           }
       }

       if (mUserName.isEmpty()) {
           if (isValid) {
               Utility.ShowAlertDialog(mActivity, "User Name", "Please enter user name");
               isValid = false;
           }
       }
       if (fName.isEmpty()) {
           if (isValid)
               Utility.ShowAlertDialog(mActivity, "First Name", "Please enter first name");
           isValid = false;

       }
       if (lName.isEmpty()) {
           if (isValid)
               Utility.ShowAlertDialog(mActivity, "Last Name", "Please enter last name");
           isValid = false;


       }
       if (eMail.isEmpty()) {
           if (isValid)
               Utility.ShowAlertDialog(mActivity, "Email", "Please enter a valid email");
           isValid = false;
       }
       if (!Utility.EmailValidator(eMail)) {
           if (isValid) {
               Utility.ShowAlertDialog(mActivity, "Email", "Please enter a valid email");
               isValid = false;
           }
       }
       if (mPassword.isEmpty()) {
           if (isValid)
               Utility.ShowAlertDialog(mActivity, "Password", "Please enter password");
           isValid = false;
       }
       if (mPassword.length()<6){
           if (isValid) {
               Utility.ShowAlertDialog(mActivity, "Password", "Password should contain at least 6 character");
               isValid = false;
           }
       }
       if (!ConfrmPASS.getText().toString().equals(mPassword))
       {
           if (isValid) {
               Utility.ShowAlertDialog(mActivity, "Confirm Password", "Password doesn’t match");
               isValid = false;
           }
       }
       if (Gym_ID==null){
           if (isValid) {
               Utility.ShowAlertDialog(mActivity, "Select Gym", "Please select a Gym");
               isValid = false;
           }
       }

       /*if (mGymName == null)
           if (isValid) {
               Utility.ShowAlertDialog(mActivity, "Select Gym", "Plz enter Gym name");
               isValid = false;
           }*/
       if (mPhoneNum.isEmpty()) {
           if (isValid)
               Utility.ShowAlertDialog(mActivity, "Phone No", "Please enter phone no");
           isValid = false;
       }


       if (dateofBirth.equals("")) {
           if (isValid)
               Utility.ShowAlertDialog(mActivity, "Date of Birth", "Please select date of birth");
           isValid = false;
       }
       if (!isChecked) {
           if (isValid)
               Utility.ShowAlertDialog(mActivity, "Terms and Conditions", "Please Accept terms and conditions");
           isValid = false;
       }
       if (isValid) {
          // Utility.OpenActivityNext(mContext, mActivity, VerificationCodeActivity.class);
           HashMap<String, String> param = new HashMap<>();
           String img="";
           if (UserImage!=null)
           {
               img=Utility.EncodeTobase64(UserImage);
           }
              
           param.put("photo", img);
           param.put("user_name", mUserName);
           param.put("first_name", fName);
           param.put("last_name", lName);
           param.put("email", eMail);
           param.put("password", mPassword);
           param.put("gym_id", Gym_ID);
           param.put("gender", mGender);
           param.put("phone", mPhoneNum);
           param.put("dob", dateofBirth);
           param.put("platform", "Android");
           param.put("deviceToken", "");
           if (!EdReff.getText().toString().trim().isEmpty())
               param.put("referral_code", EdReff.getText().toString().trim());
           new SignUp(mActivity, param).execute();
       }
   }
    private void ShowDatePicker() {
        final DatePickerDialog dpd = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
        dpd.setYearRange(1900, 2060);
        dpd.setMaxDate(calendar);
        dpd.setAccentColor(mContext.getResources().getColor(R.color.orange_ftx));
        dpd.setOnDateSetListener(this);
        FragmentManager fm = mActivity.getFragmentManager();
        dpd.show(fm, "");
    }
    @Override
    public void onDateSet(DatePickerDialog view,int year,int monthOfYear,int dayOfMonth ) {
        tv_DateOvBirth.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year));

    }


    private int getSquareCropDimensionForBitmap(Bitmap userImage) {
        return Math.min(userImage.getWidth(), userImage.getHeight());
    }


    @Override
    public void onBackPressed() {
            finish();
            mActivity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
            obj_global.SaveActiveUser(null);

    }
    public void Reigister_API(Users mUser)
    {
        Intent i = new Intent(RegisterActivity.this, VerificationCodeActivity.class);
        i.putExtra("obj", mUser);
        Utility.OpenActivityNextByData(i, mActivity);
    }
    public void API_Result(ArrayList<Gym> gymList)
    {
        mGymList = gymList;
    }

    public void Reigister_API_Status() {
        SHOWDialog();
    }

    private void SHOWDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_status_alert);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        Button btnOK = (Button) dialog.findViewById(R.id.bt_ok);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                obj_global.SaveActiveUser(null);
                Utility.OpenActivityPrevious(mContext, mActivity, LoginActivity.class);
                finish();
            }
        });
        dialog.show();
    }


    public void ShowGymsDialog() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.dialog_gym_listing);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_title);

        tvTitle.setText("Select Your Gym");

        ListView gmList = (ListView) dialog.findViewById(R.id.list_gyms);
        final GymListingAdapter adapter = new GymListingAdapter(mActivity,mGymList);
        gmList.setAdapter(adapter);

        gmList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Gym_ID=adapter.getId(position);
                mTvGymName.setText(((Gym)adapter.getItem(position)).getGym_name());
                mTvGymName.setTextColor(getResources().getColor(R.color.white));
                dialog.dismiss();
            }
        });


        ImageView mImgBack= (ImageView)dialog.findViewById(R.id.img_back);

        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        EditText mTxtSearch = (EditText) dialog.findViewById(R.id.ed_search);

        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    ArrayList<Gym> mList = new ArrayList<>();
                    for (int i = 0; i < mGymList.size(); i++) {
                        if (mGymList.get(i).getGym_name().toLowerCase().contains(s.toString().toLowerCase())) {
                            mList.add(mGymList.get(i));
                        }
                    }
                    if (mList.size() > 0) {
                        adapter.mGymList = mList;
                        adapter.notifyDataSetChanged();
                    } else {
                        adapter.mGymList = mGymList;
                        adapter.notifyDataSetChanged();
                    }
                    //adapter.getFilter().filter(arg0.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog.show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case 5:
                if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 1);
                    dialog.dismiss();
                }   else if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED )
                {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(mActivity,permissions[0]))
                    {
                        isNeverAskAgainGallery=true;
                       // ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    }
                    else
                    {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }

                break;
                case 4:
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 0);
                        dialog.dismiss();
                    }   else if(grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_DENIED )
                    {
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(mActivity,permissions[0]))
                        {
                            isNeverAskAgainCamera=true;
                            // ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                        }
                        else
                        {
                            //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                        }
                    }
                break;
        }
    }

    public void ShowPermissionWarning(final String retry, String desc)
    {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_permission_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvHeading = (TextView) dialog. findViewById(R.id.tv_heading);
        TextView tvDesc = (TextView)    dialog. findViewById(R.id.tv_desc);
        TextView tvRetry = (TextView)   dialog. findViewById(R.id.tv_retry);
        TextView tvCancel = (TextView)  dialog. findViewById(R.id.tv_cancel);

        tvHeading.setText("Location Permission Required");
        tvDesc.setText(desc);
        tvRetry.setText(retry);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(retry.equalsIgnoreCase("retry"))
                {
                    CheckPermission();
                }
                else
                {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }

                dialog.dismiss();
            }
        }); dialog.show();
    }

    private void CheckPermission() {
    }


    private void launchCamera() {
        new SandriosCamera(mActivity,null, CAPTURE_MEDIA)
                .setShowPicker(true)
           //     .setVideoFileSize(15) //File Size in MB: Default is no limit
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO) // default is CameraConfiguration.MEDIA_ACTION_BOTH
                .enableImageCropping(true) // Default is false.
                .launchCamera();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_MEDIA && resultCode == RESULT_OK) {
            try {
                UserImage = Utility.decodeScaledBitmap(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
                Log.e("File >>>>>>>>>>>>>", "" + data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
            } catch (IOException e) {
                e.printStackTrace();
            }


            if (UserImage!=null)
            {
                ImageViewProfile.setVisibility(View.VISIBLE);
                imageMassage.setVisibility(View.GONE);
                ImageViewProfile.setImageResource(0);
                ImageViewProfile.setImageBitmap(UserImage);
            }
        }
    }




}
