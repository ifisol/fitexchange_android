package ifisol.fitxchange.View.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;


/**
 * Created by MEH on 6/20/2016.
 */
public class Trial_congrats_activity extends Activity {

    Context mContext;
    Activity mActivity;
    GlobalActivity mGlobal;

    TextView tvExpDate;


    TextView tvFitXchange;
    ImageView imgTw, imgFb, imgInst;
    Button btnGotIt;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trial_congrats_wellcom);

        mContext = this;
        mActivity = this;
        mGlobal = (GlobalActivity) mActivity.getApplicationContext();


        tvExpDate    = (TextView) findViewById(R.id.tv_date);
        tvFitXchange = (TextView) findViewById(R.id.tv_fitx);
        imgFb        = (ImageView)findViewById(R.id.img_fb);
        imgTw        = (ImageView)findViewById(R.id.img_tw);
        imgInst      = (ImageView)findViewById(R.id.img_inst);
        btnGotIt     =  (Button) findViewById(R.id.btn_got_it);

        tvExpDate.setText(mGlobal.GetActiveUser().getTrial_expiry());


        tvFitXchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String url = "http://fitexchange.com.au/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        imgFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String url = "https://www.facebook.com/FitExchangeGyms/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        imgTw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String url = "https://twitter.com/fitexchange";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        imgInst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String url = "https://www.instagram.com/FitExchangeGyms/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });

        btnGotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Utility.OpenActivityNext(mContext,Trial_congrats_activity.this, MenuActivity.class);

            }
        });



    }


}
