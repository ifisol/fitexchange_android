package ifisol.fitxchange.View.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import ifisol.fitxchange.Controller.Async.ForgotPasswordTask;
import ifisol.fitxchange.Controller.Async.LoginTask;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Users;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;

/**
 * Created by MEH on 6/20/2016.
 */
public class LoginActivity extends Activity implements View.OnClickListener {
    View vRegister;
    LoginActivity loginActivity;
    Button btLogin;
    EditText edt_email,edt_password;
    TextView tvGuest,text_forgot_pass;
    Context context;
    Activity mActivity;
    public View mViewProgressBar;
    GlobalActivity mGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logintest_new);
        Init();

      /*  if (mGlobal.mLocation ==null)
            mGlobal.mLocation=new LocationClass(context);*/
    }

    private void Init() {
        context=this;
        mActivity=this;
        loginActivity=this;
        mGlobal=(GlobalActivity)context.getApplicationContext();

        if (!Utility.isConnected(context))
        {
            mGlobal.ShoeDialog(mActivity);
        }

        mViewProgressBar= findViewById(R.id.view_progrss);
        vRegister= findViewById(R.id.v_register);
        btLogin=(Button)findViewById(R.id.bt_login);
        tvGuest=(TextView)findViewById(R.id.tv_guest);
        edt_email=(EditText)findViewById(R.id.edt_email);
        edt_password=(EditText)findViewById(R.id.edt_password);
        //edt_email.setText("mehran@gmail.com");
       // edt_password.setText("1234567");
        text_forgot_pass=(TextView)findViewById(R.id.text_forgot_password);
        vRegister.setOnClickListener(this);
        text_forgot_pass.setOnClickListener(this);
        final GlobalActivity mGlobal=(GlobalActivity)context.getApplicationContext();
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GlobalActivity mGlobal = (GlobalActivity) context.getApplicationContext();
                boolean isValid = true;

                    String mEmail = edt_email.getText().toString().trim();
                    String mPassword = edt_password.getText().toString();

                    if (mEmail.isEmpty()) {
                        Utility.ShowAlertDialog(mActivity, "Username/Email", "Please enter Username or Email");
                        isValid = false;
                    }
                    /*if (!Utility.EmailValidator(mEmail)) {
                        if (isValid) {
                            Utility.ShowAlertDialog(mActivity, "Email", "Please enter a valid email");
                            isValid = false;
                        }
                    }*/

                    if (mPassword.isEmpty()) {
                        if (isValid) {
                            Utility.ShowAlertDialog(mActivity, "Password", "Please enter Password");
                            isValid = false;
                        }
                    }
                    if (mPassword.length()<6) {
                        if (isValid) {
                            Utility.ShowAlertDialog(mActivity, "Password", "Password should contain at least 6 character");
                            isValid = false;
                        }
                    }
                    if (!Utility.isConnected(context))
                    {
                        if (isValid) {
                            mGlobal.ShoeDialog(mActivity);
                            isValid = false;
                        }

                        // MenuActivity.vieWCheckInterNetConnection.setVisibility(View.VISIBLE);
                    }
                    if (isValid) {
                        HashMap<String, String> param = new HashMap<>();
                        param.put("email", mEmail);
                        param.put("password", mPassword);
                        param.put("platform", "android");

                        new LoginTask(param, loginActivity,context).execute();
                    }
                }
               /* if (!Utility.isConnected(context))
                {
                    internetCon.setVisibility(View.VISIBLE);
                }
                else
                {*/


               // Utility.OpenActivityNext(context, LoginActivity.this, MenuActivity.class);


               // }

        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.v_register:

                Utility.OpenActivityNext(context,LoginActivity.this,RegisterActivity.class);
                break;
            case R.id.text_forgot_password:
                ShowDialog();
                break;
        }

    }

    private void ShowDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_forgot_password);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        final EditText edt_mail=(EditText)dialog.findViewById(R.id.edt_eemail);
        Button mBttonok = (Button) dialog.findViewById(R.id.bt_ok);
        Button mBttonCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        mBttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        mBttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mail=edt_mail.getText().toString();
                if (mail.isEmpty()) {
                    Utility.ShowAlertDialog(mActivity, "User Email", "Please enter a valid email");
                }
                else {
                    if (Utility.EmailValidator(mail))
                    {
                        HashMap<String, String> param = new HashMap<>();
                        param.put("email", mail);
                        new ForgotPasswordTask(param, loginActivity).execute();
                        dialog.dismiss();
                    }
                    else
                    {
                        Utility.ShowAlertDialog(mActivity, "Email", "Please enter a valid email");
                    }
                }
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        Utility.OpenActivityPrevious(context, this, DemoActivity.class);
    }

    public void Api_Result(Users user)
    {
        if (user.getOn_contract().equals("yes"))
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date ExpDate = sdf.parse(user.getContract_expiry());
                if (new Date().after(ExpDate)) {
                    ExpiredDialog(mActivity);
                    return;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        mGlobal.SaveActiveUser(user);
        if (user.getOn_trial().equals("yes"))
            Utility.OpenActivityNext(context,LoginActivity.this, Trial_congrats_activity.class);
        else
            Utility.OpenActivityNext(context, LoginActivity.this, MenuActivity.class);

    }

    public void Api_Result_toRegisterActivity(Users mUser)
    {
        Intent i = new Intent(LoginActivity.this, VerificationCodeActivity.class);
        i.putExtra("obj", mUser);
        Utility.OpenActivityNextByData(i,mActivity);
    }

    public void Api_Result_Status() {
        ShowDIALOG();
    }

    private void ShowDIALOG() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_status_alert_login);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        Button btnOK = (Button) dialog.findViewById(R.id.bt_ok);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void ExpiredDialog(Activity mActivity) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.trial_expired_dialog);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        TextView btnClose = (TextView) dialog.findViewById(R.id.bt_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        dialog.show();
    }

}
