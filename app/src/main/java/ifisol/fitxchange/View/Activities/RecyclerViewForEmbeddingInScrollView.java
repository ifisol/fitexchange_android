package ifisol.fitxchange.View.Activities;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by MEH on 7/15/2016.
 */
public class RecyclerViewForEmbeddingInScrollView extends RecyclerView {
    public RecyclerViewForEmbeddingInScrollView(Context context) {
        super(context);
    }

    public RecyclerViewForEmbeddingInScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerViewForEmbeddingInScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return false;
    }
}
