package ifisol.fitxchange.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.twitter.sdk.android.tweetcomposer.TweetUploadService;

import java.io.File;

import ifisol.fitxchange.Model.Constants;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.R;
import io.fabric.sdk.android.Fabric;

/**
 * Created by MEH on 10/4/2016.
 */
public class TwitterActivity extends Activity {
    private static final String TWITTER_KEY = "sOTGWtSKf6uIEJ581SgWDLNhm";
    private static final String TWITTER_SECRET = "XyEzxD4lta1pifitO0pugl3hBC34VwR4Jpnu8TcNWq6pKhSPNU";
    private TwitterLoginButton twitterButton;
    Context context;
    Gym gym;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.twitter_activity);
        Intent i = getIntent();
        context=this;
        gym=(Gym)i.getSerializableExtra("Object");
        String photo=gym.getGym_logo();
        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text("Training at my other gym with Fit Exchange @fitexchangegyms #fitexchange");
        builder.show();
        setUpViews();
    }

    private void setUpViews() {
        setUpTwitterButton();
    }

    private void setUpTwitterButton() {
        twitterButton = (TwitterLoginButton) findViewById(R.id.twitter_button);
        twitterButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Toast.makeText(TwitterActivity.this, "You shared post successfully", Toast.LENGTH_SHORT).show();
                Log.d("rht", "Invitation Sent Successfully");
                //setUpViewsForTweetComposer();

            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(getApplicationContext(), "Post Cancel", Toast.LENGTH_SHORT).show();
            }
        });

    }
/*
    private void setUpViewsForTweetComposer() {
        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text("Fit Exchange: Champions aren't born.They're made");
        builder.show();

    }*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       /* super.onActivityResult(requestCode, resultCode, data);*/
        twitterButton.onActivityResult(requestCode, resultCode, data);
    }
}