package ifisol.fitxchange.View.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baoyz.actionsheet.ActionSheet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.Model.Menuu;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;
import ifisol.fitxchange.View.Fragments.CodesFragment;
import ifisol.fitxchange.View.Fragments.DiscoverFragment;
import ifisol.fitxchange.View.Fragments.DiscoverMainFragment;
import ifisol.fitxchange.View.Fragments.GymDetailFragment;
import ifisol.fitxchange.View.Fragments.InfoFragment;
import ifisol.fitxchange.View.Fragments.ProfileFragment;
import ifisol.fitxchange.View.Fragments.SettingsFragment;

/**
 * Created by MEH on 6/22/2016.
 */
public class MenuActivity extends FragmentActivity implements OnClickListener{

    Context context;
    Activity mActivity;
    GlobalActivity mGlobal;
    Fragment fragmentInstance;

    FrameLayout frameLayout;
    View mViewDiscover,mViewCode,mViewProfile;

    public static ImageView imgMenu,img_back_gymdetail;
    public static ImageView imgFilter;
    public static ImageView imgHome;
    public static ImageView imgSettings;
    public static View mViewBottom;
    public static View mViewProgressBar;
    public static TextView header;
    public static ImageView imgInfo;
    public static View vieWCheckInterNetConnection;

    ImageView imgBack;
    View vRoot;
    CircleImageView circleImage;
    TextView tvRegisterOrSignOut;
    LinearLayout layoutFilterMenuList;
    public boolean isFilterMenuShowing;


    ArrayList<Menuu> mArray;

    ImageView img_discover,img_code,img_profile;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Init();


        if (mGlobal.GetActiveUser()!=null && mGlobal.GetActiveUser().getIs_subscribed().equalsIgnoreCase("no") && mGlobal.GetActiveUser().getOn_trial().equals("no"))
        {
            Utility.ReplaceFragment(new SettingsFragment(), getSupportFragmentManager());
        }
        else
        {
            Utility.ReplaceFragment(new DiscoverMainFragment(), getSupportFragmentManager());
        }

    }

    private void Init() {
        context = this;
        mActivity=this;
        mGlobal= (GlobalActivity) context.getApplicationContext();
        mGlobal.isMenuActivty=true;

        imgInfo=(ImageView) findViewById(R.id.img_info);
        frameLayout = (FrameLayout) findViewById(R.id.fram);
        vieWCheckInterNetConnection= findViewById(R.id.view_internetConnection);
        mViewProgressBar= findViewById(R.id.view_progrssbar);
        imgFilter = (ImageView) findViewById(R.id.imag_filter);
        imgHome  = (ImageView) findViewById(R.id.img_home);
        imgSettings  = (ImageView) findViewById(R.id.img_settings);
        header = (TextView) findViewById(R.id.tv_header);
        imgBack = (ImageView) findViewById(R.id.img_back);
        img_back_gymdetail = (ImageView) findViewById(R.id.img_backk);
        vRoot= findViewById(R.id.root_view);
        mViewBottom= findViewById(R.id.view_bottomm);
        mViewDiscover= findViewById(R.id.view_discover);
        mViewCode= findViewById(R.id.view_code);
        mViewProfile= findViewById(R.id.view_profile);
        circleImage = (CircleImageView) findViewById(R.id.jymprofile_image);
        tvRegisterOrSignOut = (TextView) findViewById(R.id.tv_register);
        img_discover=(ImageView)findViewById(R.id.img_discver);
        img_code=(ImageView)findViewById(R.id.img_code);
        img_profile=(ImageView)findViewById(R.id.img_profile);
        mArray = new ArrayList<Menuu>();
        isFilterMenuShowing = false;
        mViewDiscover.setOnClickListener(this);
        mViewCode.setOnClickListener(this);
        mViewProfile.setOnClickListener(this);
        imgFilter.setOnClickListener(this);
        imgSettings.setOnClickListener(this);
        imgHome.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        fragmentInstance = getSupportFragmentManager().findFragmentById(R.id.fram);
        switch (v.getId()) {

            case R.id.img_settings:
                if (!Utility.isConnected(context))
                {
                    mGlobal.ShoeDialog(mActivity);
                }
                else  {
                    Utility.ReplaceFragment(new SettingsFragment(), getSupportFragmentManager());
                }

                break;

            case R.id.imag_filter:
                if (!Utility.isConnected(context))
                {
                    mGlobal.ShoeDialog(mActivity);
                }
                else  {
                        ShowBottomFilterManu();
                    }
                break;

            case R.id.view_discover:

                imgHome.setVisibility(View.GONE);
                imgSettings.setVisibility(View.GONE);
                imgInfo.setVisibility(View.GONE);

                if (mGlobal.GetActiveUser()!=null) {
                    if (fragmentInstance instanceof DiscoverMainFragment)
                    {

                    }
                    else
                    {
                        if (!Utility.isConnected(context))
                        {
                            mGlobal.ShoeDialog(mActivity);
                        }
                        else
                        {
                            Utility.ReplaceFragment(new DiscoverMainFragment(), getSupportFragmentManager());
                            UpDate("discover");
                        }


                    }
                }

                break;
            case R.id.view_code:

                imgHome.setVisibility(View.GONE);
                imgSettings.setVisibility(View.GONE);
                imgInfo.setVisibility(View.GONE);

                if (mGlobal.GetActiveUser()!=null) {
                    if (fragmentInstance instanceof CodesFragment)
                    {

                    }
                    else
                    {
                        if (!Utility.isConnected(context))
                        {
                            mGlobal.ShoeDialog(mActivity);                        }
                        else {
                            Utility.ReplaceFragment(new CodesFragment(), getSupportFragmentManager());
                            UpDate("code");
                        }

                    }

                }
                else
                {
                    ShowDialogRegisterUser();
                }
                break;
            case R.id.view_profile:

                if (mGlobal.GetActiveUser()!=null) {
                    if (fragmentInstance instanceof ProfileFragment)
                    {

                    }
                    else
                    {
                        if (!Utility.isConnected(context))
                        {
                            mGlobal.ShoeDialog(mActivity);                        }
                        else
                        {
                            Utility.ReplaceFragment(new ProfileFragment(), getSupportFragmentManager());
                            UpDate("profile");
                        }


                    }
                }
                else
                {
                    ShowDialogRegisterUser();
                }
                break;
            case R.id.img_home:
                Utility.SHOWhOMEDialog(mActivity);
                break;

        }


    }
    public static Comparator<Gym> NameComparator = new Comparator<Gym>() {

        public int compare(Gym gym1, Gym gym2) {

            String gym_name1 = gym1.getGym_name();
            String gym_name2 = gym2.getGym_name();
            return gym_name1.compareToIgnoreCase(gym_name2);
        }

    };
    public static Comparator<Gym> DistanceComparator = new Comparator<Gym>() {

        public int compare(Gym gym1, Gym gym2) {
            double mvalue = Double.parseDouble( String.valueOf(gym1.getDistance_in_km()) );
            double mvalue2 = Double.parseDouble( String.valueOf(gym2.getDistance_in_km()) );
            return Double.compare(mvalue,mvalue2);
        }

    };

    private void ShowBottomFilterManu() {

        ActionSheet.createBuilder(context,getSupportFragmentManager())
                .setCancelButtonTitle("Cancel")
                .setOtherButtonTitles("Name", "Closest")
                .setCancelableOnTouchOutside(true)
                .setListener(new ActionSheet.ActionSheetListener() {
                    @Override
                    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

                    }

                    @Override
                    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
                        fragmentInstance = getSupportFragmentManager().findFragmentById(R.id.fram_layout);
                        switch (index) {
                            case 0:
                                if (fragmentInstance instanceof DiscoverFragment)
                                Collections.sort(((DiscoverFragment) fragmentInstance).discoverAdopter.mArray, NameComparator);
                                break;
                            case 1:
                                Collections.sort(((DiscoverFragment) fragmentInstance).discoverAdopter.mArray, DistanceComparator);
                                break;
                        }
                        ((DiscoverFragment) fragmentInstance).discoverAdopter.notifyDataSetChanged();

                    }
                }).show();
    }

    private void closeMyView() {
        layoutFilterMenuList.setVisibility(View.GONE);
        imgFilter.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        imgFilter.setColorFilter(ContextCompat.getColor(context, R.color.black));
        isFilterMenuShowing = false;
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fram);
        if(mGlobal.GetActiveUser()==null)
        {
            if (fragment instanceof GymDetailFragment)
            {
                getSupportFragmentManager().popBackStack();
            }
            else {

                Utility.OpenActivityPrevious(context, this, DemoActivity.class);
            }

        }
        else {
            if (fragment instanceof DiscoverFragment)
            {
                if (mGlobal.GetActiveUser()!=null) {
                }
                else {
                    finish();
                    Utility.OpenActivityPrevious(context, this, DemoActivity.class);
                }

            }
            if (fragment instanceof InfoFragment)
            {
                getSupportFragmentManager().popBackStack();
            }
            else if (fragment instanceof GymDetailFragment)
            {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    private void ShowDialogRegisterUser() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_register_user);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        Button mbtRegister = (Button) dialog.findViewById(R.id.bt_gallery);
        Button mbtCancel = (Button) dialog.findViewById(R.id.bt_camera);
        mbtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        mbtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.OpenActivityPrevious(context, MenuActivity.this, RegisterActivity.class);
            }
        });
        dialog.show();
    }

    public void UpDate(String var) {
        if (var.equalsIgnoreCase("discover"))
        {
            mViewDiscover.setBackgroundColor(getResources().getColor(R.color.orange_ftx));
           img_discover.setImageResource(R.drawable.explore_click);
            mViewCode.setBackgroundColor(getResources().getColor(R.color.white));
           img_code.setImageResource(R.drawable.code_unclick);
            mViewProfile.setBackgroundColor(getResources().getColor(R.color.white));
           img_profile.setImageResource(R.drawable.profile_unclick);
        }
        if (var.equalsIgnoreCase("code"))
        {
            mViewCode.setBackgroundColor(getResources().getColor(R.color.orange_ftx));
           img_code.setImageResource(R.drawable.code_click);
            img_discover.setImageResource(R.drawable.explore_unclick);
            mViewDiscover.setBackgroundColor(getResources().getColor(R.color.white));
            mViewProfile.setBackgroundColor(getResources().getColor(R.color.white));
            img_profile.setImageResource(R.drawable.profile_unclick);
        }
        if (var.equalsIgnoreCase("profile"))
        {
            mViewProfile.setBackgroundColor(getResources().getColor(R.color.orange_ftx));
            img_profile.setImageResource(R.drawable.profile_click);
            mViewDiscover.setBackgroundColor(getResources().getColor(R.color.white));
            img_discover.setImageResource(R.drawable.explore_unclick);
            mViewCode.setBackgroundColor(getResources().getColor(R.color.white));
            img_code.setImageResource(R.drawable.code_unclick);
        }

    }

    @Override
    protected void onDestroy() {
        mGlobal.isMenuActivty=false;
        super.onDestroy();
    }
}


