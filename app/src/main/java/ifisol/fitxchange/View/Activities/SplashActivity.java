package ifisol.fitxchange.View.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.LocationClass;
import ifisol.fitxchange.Utilities.Utility;


/**
 * Created by MEH on 6/20/2016.
 */
public class SplashActivity extends Activity {
    SplashActivity mActivity;
    Context mContext;
    GlobalActivity mGlobal;
    LocationManager mLocationManager;
    boolean isLocationNeverAsk=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext=this;
        mActivity=this;
       // LocationClass.getInstance(mActivity, mContext);
        mGlobal=(GlobalActivity)mContext.getApplicationContext();
        mActivity=this;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CheckPermission();
        }
        else {
            MoveToNext();
        }



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {

            case 6:
            {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    MoveToNext();
                }
                else if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED )
                {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(mActivity,permissions[0]))
                    {
                        isLocationNeverAsk=true;
                        ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services.Fit Exchange wont be able to work without location permission. ");

                    }
                    else
                    {

                        ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services.Fit Exchange wont be able to work without location permission. ");

                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    private void MoveToNext() {
        LocationClass.getInstance(mActivity, mContext);
        final Handler handl=new Handler();
        handl.postDelayed(new Runnable() {
            @Override
            public void run() {
                GlobalActivity mGlobal = (GlobalActivity) mContext.getApplicationContext();
                if (mGlobal.GetActiveUser() == null) {
                    Utility.OpenActivityNext(mContext, SplashActivity.this, DemoActivity.class);
                } else {

                    Utility.OpenActivityNext(mContext, SplashActivity.this, MenuActivity.class);
                }
            }

        }, 2000);
    }
   public void ShowPermissionWarning(final String retry, String desc)
   {
       final Dialog dialog = new Dialog(mActivity);
       dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
       dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
       dialog.setContentView(R.layout.custom_permission_dialog);
       dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
       dialog.setCancelable(false);
       DisplayMetrics dm = new DisplayMetrics();
       mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
       WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
       Window window = dialog.getWindow();
       window.setGravity(Gravity.CENTER | Gravity.START);
       lp.copyFrom(window.getAttributes());
       lp.width = WindowManager.LayoutParams.MATCH_PARENT;
       lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
       window.setAttributes(lp);
       TextView tvHeading = (TextView) dialog. findViewById(R.id.tv_heading);
       TextView tvDesc = (TextView)    dialog. findViewById(R.id.tv_desc);
       TextView tvRetry = (TextView)   dialog. findViewById(R.id.tv_retry);
       TextView tvCancel = (TextView)  dialog. findViewById(R.id.tv_cancel);

       tvHeading.setText("Location Permission Required");
       tvDesc.setText(desc);
       tvRetry.setText(retry);

       tvCancel.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               dialog.dismiss();
               finishAffinity();
           }
       });

       tvRetry.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               if(retry.equalsIgnoreCase("retry"))
               {
                   CheckPermission();
               }
               else
               {
                   Intent intent = new Intent();
                   intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                   Uri uri = Uri.fromParts("package", getPackageName(), null);
                   intent.setData(uri);
                   startActivity(intent);
               }

               dialog.dismiss();
           }
       }); dialog.show();
   }

    private void CheckPermission() {
        if (ContextCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED)
        {
            if (isLocationNeverAsk)
            {
                ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
            }
            else
                ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 6);
        }
        else
        {
            MoveToNext();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CheckPermission();
        }
        else {
            MoveToNext();
        }
    }
}
