package ifisol.fitxchange.View.Activities;

import android.content.Context;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ifisol.fitxchange.Controller.Adapters.DemoAdapter;
import ifisol.fitxchange.Global.GlobalActivity;
import ifisol.fitxchange.R;
import ifisol.fitxchange.Utilities.Utility;

/**
 * Created by MEH on 6/20/2016.
 */
public class DemoActivity extends FragmentActivity implements View.OnClickListener {
    ViewPager view_pager;
    View vCnter,vLeft,vRight;
    Button btLogin,btSignUp;
    Context mContext;
    TextView mtvGuest;
    GlobalActivity mGlobal;
    SoundPool soundPool;
    FragmentPagerAdapter adapterViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        Init();
        adapterViewPager = new DemoAdapter(getSupportFragmentManager());
        view_pager.setAdapter(adapterViewPager);
        view_pager.setCurrentItem(1);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        vLeft.setBackground(getResources().getDrawable(R.drawable.circle_white));
                        vCnter.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                        vRight.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                        break;

                    case 1:
                        vLeft.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                        vCnter.setBackground(getResources().getDrawable(R.drawable.circle_white));
                        vRight.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                        break;
                    case 2:
                        vLeft.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                        vCnter.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                        vRight.setBackground(getResources().getDrawable(R.drawable.circle_white));
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    private void Init() {
        mContext=this;
        mGlobal=(GlobalActivity)mContext.getApplicationContext();

        view_pager=(ViewPager)findViewById(R.id.vPager);
        vCnter= findViewById(R.id.v_centr);
        vLeft= findViewById(R.id.v_left);
        vRight= findViewById(R.id.v_right);
        btLogin=(Button)findViewById(R.id.bt_login_demo);
        btSignUp=(Button)findViewById(R.id.bt_signup_demo);
        mtvGuest=(TextView)findViewById(R.id.tv_guest);
        mtvGuest.setOnClickListener(this);
        btLogin.setOnClickListener(this);
        btSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_login_demo:
                Utility.OpenActivityNext(mContext,DemoActivity.this,LoginActivity.class);
                break;
            case R.id.bt_signup_demo:
                Utility.OpenActivityNext(mContext,DemoActivity.this,RegisterActivity.class);
                break;
            case R.id.tv_guest:
                Utility.OpenActivityNext(mContext,DemoActivity.this,MenuActivity.class);
        }
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }


}
