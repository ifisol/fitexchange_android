package ifisol.fitxchange.Utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import ifisol.fitxchange.Global.GlobalActivity;

/**
 * Created by MEH on 11/28/2016.
 */
public class LocationClass implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
        , LocationListener {
    Activity mActivity;
    GlobalActivity mGlobal;
    private Context mContext;
    private static final String TAG = "Location Class";
    private static final long INTERVAL = 1000 * 50;
    private static final long FASTEST_INTERVAL = 1000 * 15;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    String address = "";

    static LocationClass obj;

    public static LocationClass getInstance(Activity activity, Context context) {
        if (obj == null)
            obj = new LocationClass(activity, context);

        return obj;
    }

    private LocationClass(Activity activity, Context context) {
        this.mContext = context;
        mActivity = activity;
        mGlobal = (GlobalActivity) mActivity.getApplicationContext();
        createLocationRequest();
        CreateGoogleClientAPI();

        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void CreateGoogleClientAPI() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }


    protected void startLocationUpdates() {
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }


    @Override
    public void onConnected(Bundle bundle) {

        startLocationUpdates();
        Log.d(TAG, "Connected ..............: ");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed .......................");
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d(TAG, "Firing onLocationChanged...." + location + ".........................................");
        setmCurrentLocation(location);
        String lat= Double.toString(location.getLatitude());
        String lon= Double.toString(location.getLongitude());
        String locationn=lat+","+lon;
        mGlobal.SaveLocation(locationn);
      /*  SharedPreferences mPreferencemPreference = mContext.getSharedPreferences("MY_PREF", mContext.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mPreferencemPreference.edit();
        mEditor.putString("location", String.valueOf(location));
        mEditor.commit();*/



    }

    public void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        Log.d(TAG, "Location update stopped .......................");
    }

    public Location getmCurrentLocation() {

        return mCurrentLocation;
    }

    public void setmCurrentLocation(Location mCurrentLocation) {

        this.mCurrentLocation = mCurrentLocation;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }
}
