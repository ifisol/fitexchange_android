package ifisol.fitxchange.Global;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ifisol.fitxchange.Model.Gym;
import ifisol.fitxchange.Model.Services;
import ifisol.fitxchange.Model.Users;
import ifisol.fitxchange.R;

/**
 * Created by MEH on 6/23/2016.
 */
public class GlobalActivity extends Application{

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    public boolean isMenuActivty;
    SharedPreferences mPreference;
    SharedPreferences.Editor mEditor;

    public void  ShoeDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_internetconnection_dialog);
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.CENTER);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        Button mbtCancel = (Button) dialog.findViewById(R.id.bt_cancell);
        mbtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void onCreate() {
        super.onCreate();

        mPreference = getSharedPreferences("MY_PREF", Context.MODE_PRIVATE);
        mEditor = mPreference.edit();
            // Initialize the SDK before executing any other operations,
            FacebookSdk.sdkInitialize(getApplicationContext());
            AppEventsLogger.activateApp(this);
    }
    public void SaveActiveUser(Users user) {

        Gson gson=new Gson();
        String mUser=gson.toJson(user,Users.class);
        mEditor.putString("ACTIVE_USER",mUser);
        mEditor.commit();


    }



    public Users GetActiveUser() {

        Gson gson=new Gson();
        String user=mPreference.getString("ACTIVE_USER", null);
        Users mUser=gson.fromJson(user,Users.class);
        return mUser;
    }

    public void SaveLocation(String loc)
    {
        mEditor.putString("location", String.valueOf(loc));
        mEditor.commit();
    }

    public String getLocation()
    {
        String loc=mPreference.getString("location", null);
        return loc;
    }
    public void SaveCode(String code) {

        mEditor.putString("CODE",code);
        mEditor.commit();
    }
    public String GetCode() {

        String code=mPreference.getString("CODE", null);
        return code;
    }

    public void SaveGym(Gym gym) {

        Gson gson=new Gson();
        String mGym=gson.toJson(gym,Gym.class);
        mEditor.putString("GYM",mGym);
        mEditor.commit();
    }

    public Gym GetGym() {

        Gson gson=new Gson();
        String gym=mPreference.getString("GYM", null);
        Gym mGym=gson.fromJson(gym, Gym.class);
        return mGym;
    }

    public void saveShared(boolean isSaved) {

        mEditor.putBoolean("IS_SAVED",isSaved);
        mEditor.commit();
    }

    public boolean GetIsShared() {

        return mPreference.getBoolean("IS_SAVED", false);
    }

}
