package ifisol.fitxchange.json;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import ifisol.fitxchange.Model.Constants;

public class JSONArrayParser {
    static InputStream is = null;
    static String response = "";

    // constructor
    public JSONArrayParser() {

    }

    public String getJSONFromUrl(HashMap<String, String> params) {

        response = "";

        // Making HTTP request
        HttpURLConnection urlc = null;
        try {
            urlc = (HttpURLConnection) (new URL(Constants.URL+getPostDataString(params)).openConnection());
            urlc.setReadTimeout(50000);
            urlc.setConnectTimeout(50000);
            urlc.setRequestMethod("GET");
            urlc.setDoInput(true);
            urlc.connect();
            int responseCode=urlc.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(urlc.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else {
                response= "Internet connection lost. Please try again later.";
            }
            Log.d("Response code", String.valueOf(responseCode));
        } catch (IOException e) {
           Log.d("Exception", e.getMessage()) ;
            response= "Internet connection lost. Please try again later.";
        }

        return response;
    }

    public String getJSONFromUrl_POST(HashMap<String, String> params, String webUrl) {
        Log.d("URL", Constants.URL+webUrl);
        response = "";

        // Making HTTP request
        HttpURLConnection urlc = null;
        try {
            urlc = (HttpURLConnection) (new URL(Constants.URL+webUrl).openConnection());
            urlc.setReadTimeout(40000);
            urlc.setConnectTimeout(40000);
            urlc.setRequestMethod("POST");
            urlc.setDoInput(true);
            urlc.setDoOutput(true);
            OutputStream os = urlc.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));
            writer.flush();
            writer.close();
            os.close();
            urlc.connect();
            int responseCode=urlc.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(urlc.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else {
                Log.d("aeRRRRORRRRR", urlc.getErrorStream().toString() );
                response= "Internet connection lost. Please try again later.";
            }
            Log.d("Response Code", String.valueOf(responseCode)) ;
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("Exception", e.getMessage()) ;
            response= "Internet connection lost. Please try again later.";
        }

        return response;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }
}



