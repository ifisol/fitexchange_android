package ifisol.fitxchange.Model;

import java.io.Serializable;

/**
 * Created by MEH on 7/25/2016.
 */
public class Users implements Serializable {


    String user_id;
    String user_type;
    String user_name;
    String first_name;
    String last_name;
    String email;
    String password;
    String gender;
    String phone;
    String dob;
    String platform;
    String deviceToken;
    String photo;
    String reg_code;
    String is_subscribed;
    String on_trial;
    public String gym_name;
    public String gym_id;
    private String trial_expiry;
    String on_contract;
    String contract_expiry;
    String contract_start;


    public String getOn_contract() {

        return on_contract;
    }

    public void setOn_contract(String on_contract) {

        this.on_contract = on_contract;
    }

    public String getContract_expiry() {

        return contract_expiry;
    }

    public void setContract_expiry(String contract_expiry) {

        this.contract_expiry = contract_expiry;
    }

    public String getContract_start() {

        return contract_start;
    }

    public void setContract_start(String contract_start) {

        this.contract_start = contract_start;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getVerification_status() {
        return verification_status;
    }

    public void setVerification_status(String verification_status) {
        this.verification_status = verification_status;
    }

    public String verification_status;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getReg_code() {
        return reg_code;
    }

    public void setReg_code(String reg_code) {
        this.reg_code = reg_code;
    }

    public String getGym_name() {
        return gym_name;
    }

    public void setGym_name(String gym_name) {
        this.gym_name = gym_name;
    }

    public String getGym_id() {
        return gym_id;
    }

    public void setGym_id(String gym_id) {
        this.gym_id = gym_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getIs_subscribed() {

        return is_subscribed;
    }

    public void setIs_subscribed(String is_subscribed) {

        this.is_subscribed = is_subscribed;
    }

    public String getOn_trial() {

        return on_trial;
    }

    public void setOn_trial(String on_trial) {

        this.on_trial = on_trial;
    }

    public String getTrial_expiry() {

        return trial_expiry;
    }

    public void setTrial_expiry(String trial_expiry) {

        this.trial_expiry = trial_expiry;
    }
}
