package ifisol.fitxchange.Model;

/**
 * Created by MEH on 8/10/2016.
 */
public class Info {
    public String agreement_text;
    public String video_url;

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getAgreement_text() {
        return agreement_text;
    }

    public void setAgreement_text(String agreement_text) {
        this.agreement_text = agreement_text;
    }


}
