package ifisol.fitxchange.Model;

import java.io.Serializable;

/**
 * Created by MEH on 8/1/2016.
 */
public class Services implements Serializable {

    public String feature_name;
    public String feature_img;
    public String feature_id;

    public String getFeature_name() {
        return feature_name;
    }

    public String getFeature_img() {
        return feature_img;
    }

    public void setFeature_img(String feature_img) {
        this.feature_img = feature_img;
    }

    public void setFeature_name(String feature_name) {
        this.feature_name = feature_name;
    }


    public String getFeature_id() {
        return feature_id;
    }

    public void setFeature_id(String feature_id) {
        this.feature_id = feature_id;
    }
}