package ifisol.fitxchange.Model;

import java.io.Serializable;

/**
 * Created by MEH on 8/10/2016.
 */
public class Timings implements Serializable {

     String day;
     String open_timing;
     String close_timing;

    public String getOpen_timing() {
        return open_timing;
    }

    public void setOpen_timing(String open_timing) {
        this.open_timing = open_timing;
    }

    public String getClose_timing() {
        return close_timing;
    }

    public void setClose_timing(String close_timing) {
        this.close_timing = close_timing;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
