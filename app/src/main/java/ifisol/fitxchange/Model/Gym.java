package ifisol.fitxchange.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by MEH on 7/25/2016.
 */
public class Gym implements Serializable {

    String               Code_Date_Time_Creation;
    String               gym_name;
    String               gym_id;
    String               gym_logo;
    String               gym_address;
    String               distance_in_km;
    String               gym_phone;
    String               gym_website;
    String               latitude;
    String               longitude;
    String               isSplitHours;
    String               within_range;
    ArrayList<Services>  gym_features;
    ArrayList<Timings>   gym_timings;
    String on_contract;
    String contract_expiry;
    String contract_start;

    public String getContract_start() {

        return contract_start;
    }

    public void setContract_start(String contract_start) {

        this.contract_start = contract_start;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }



    public String getDistance_in_km() {
        return distance_in_km;
    }

    public void setDistance_in_km(String distance_in_km) {
        this.distance_in_km = distance_in_km;
    }

    public String getCode_Date_Time_Creation() {
        return Code_Date_Time_Creation;
    }

    public void setCode_Date_Time_Creation(String code_Date_Time_Creation) {
        Code_Date_Time_Creation = code_Date_Time_Creation;
    }

    public String getGym_name() {
        return gym_name;
    }

    public void setGym_name(String gym_name) {
        this.gym_name = gym_name;
    }

    public String getGym_id() {
        return gym_id;
    }

    public void setGym_id(String gym_id) {
        this.gym_id = gym_id;
    }

    public String getGym_logo() {
        return gym_logo;
    }

    public void setGym_logo(String gym_logo) {
        this.gym_logo = gym_logo;
    }

    public String getGym_address() {
        return gym_address;
    }

    public void setGym_address(String gym_address) {
        this.gym_address = gym_address;
    }


    public String getGym_phone() {
        return gym_phone;
    }

    public void setGym_phone(String gym_phone) {
        this.gym_phone = gym_phone;
    }

    public String getGym_website() {
        return gym_website;
    }

    public void setGym_website(String gym_website) {
        this.gym_website = gym_website;
    }

    public ArrayList<Services> getGym_features() {
        return gym_features;
    }

    public void setGym_features(ArrayList<Services> gym_features) {
        this.gym_features = gym_features;
    }

    public ArrayList<Timings> getGym_timings() {
        return gym_timings;
    }

    public void setGym_timings(ArrayList<Timings> gym_timings) {
        this.gym_timings = gym_timings;
    }

    public String getIsSplitHours() {

        return isSplitHours;
    }

    public void setIsSplitHours(String isSplitHours) {

        this.isSplitHours = isSplitHours;
    }

    public String getOn_contract() {

        return on_contract;
    }

    public void setOn_contract(String on_contract) {

        this.on_contract = on_contract;
    }

    public String getContract_expiry() {

        return contract_expiry;
    }

    public void setContract_expiry(String contract_expiry) {

        this.contract_expiry = contract_expiry;
    }

    public String getWithin_range() {

        return within_range;
    }

    public void setWithin_range(String within_range) {

        this.within_range = within_range;
    }
}
