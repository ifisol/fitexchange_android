package ifisol.fitxchange.Model;

/**
 * Created by Mehtab Ahmad on 11/14/2016.
 */
public class Subscription_Plans
{

    String plan_id;
    String plan_name;
    String plan_price;
    String plan_duration;
    String plan_description;
    String is_subscribed;
    String can_un_subscribe;

    public String getCan_un_subscribe() {
        return can_un_subscribe;
    }

    public void setCan_un_subscribe(String can_un_subscribe) {
        this.can_un_subscribe = can_un_subscribe;
    }

    public String getPlan_id() {

        return plan_id;
    }

    public void setPlan_id(String plan_id) {

        this.plan_id = plan_id;
    }

    public String getPlan_name() {

        return plan_name;
    }

    public void setPlan_name(String plan_name) {

        this.plan_name = plan_name;
    }

    public String getPlan_price() {

        return plan_price;
    }

    public void setPlan_price(String plan_price) {

        this.plan_price = plan_price;
    }

    public String getPlan_duration() {

        return plan_duration;
    }

    public void setPlan_duration(String plan_duration) {

        this.plan_duration = plan_duration;
    }

    public String getPlan_description() {

        return plan_description;
    }

    public void setPlan_description(String plan_description) {

        this.plan_description = plan_description;
    }

    public String getIs_subscribed() {

        return is_subscribed;
    }

    public void setIs_subscribed(String is_subscribed) {

        this.is_subscribed = is_subscribed;
    }
}
