package ifisol.fitxchange.Model;

/**
 * Created by MEH on 7/25/2016.
 */
public class Constants {
 //  public static String URL = "http://fitexchange.com.au/gym/api/";
   public static String URL = "http://test.ifisol.com/api/";
    public static String VERIFY_USER = "verify_user";
    public static String LOGIN = "gym_login";
    public static String SIGNUP = "singup_user";
    public static String FORGOT_PASSWORD ="forgot_password";
    public static String Search_gym = "search_gym";
    public static String Gym_Info = "user_gym_info";
    public static String CHANG_PASSWORD ="change_password";
    public static String UPDATE_Profile = "update_user_profile";
    public static String FIT_GYM_NAME_LIST = "fit_gym_name_list";
    public static String DELETE_USER = "delete_user";
    public static String RESEND_CODE = "resend_verification_code";
    public static String TRAIN_NOW = "train_now";
    public static String PREVIOUS_VISIT = "get_visitors_gym";
    public static String INFO = "get_gym_info";
    public static String Get_Token = "send_clientToken";
    public static String CreateSubscription = "create_subscription";
    public static String Get_plans = "get_plans";
  //  public static String Img_URL = "http://fitexchange.com.au/gym/";
    public static String Img_URL = "http://test.ifisol.com/";
}
